var webpack = require('webpack');
var path = require('path');
require('es6-promise').polyfill();

var APP_DIR = path.resolve(__dirname, 'src/main/resources');
var BUILD_DIR = path.resolve(__dirname, 'src/main/resources/bookstore');

var config = {
    entry: ['babel-polyfill', 'whatwg-fetch', path.resolve(APP_DIR + '/index.jsx')],
    output: {
        path: BUILD_DIR,
        filename: 'bundle.js',
        sourceMapFilename: 'bundle.map.js'
    },

    module : {
        loaders : [
            {
                test : /\.jsx?/,
                include : APP_DIR,
                loader : 'babel-loader'
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
            {
                test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
                loader: 'url-loader',
                options: {
                    limit: 10000
                }
            }
        ]
    }
};

module.exports = config;