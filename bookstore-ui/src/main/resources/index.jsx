import React from 'react';
import {render} from 'react-dom';
import {
    HashRouter as Router,
    Route,
    Switch
} from 'react-router-dom'

import OrderGrid from './components/OrderGrid.jsx';
import OrderDetail from './components/OrderDetail.jsx';
import DeliveryGrid from './components/DeliveryGrid.jsx';
import DeliveryDetail from './components/DeliveryDetail.jsx'
import Login from './components/Login.jsx';

import '../../../node_modules/bootstrap/dist/css/bootstrap.css'
import '../../../node_modules/fixed-data-table-2/dist/fixed-data-table.min.css'
import './css/bookstore.css'

class App extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Router>
                <div>
                    <Switch>
                        <Route exact path="/login" component={Login}/>
                        <Route exact path="/" component={OrderGrid}/>
                        <Route exact path="/order/:id" component={OrderDetail}/>
                        <Route exact path="/delivery" component={DeliveryGrid}/>
                        <Route exact path="/delivery/:id" component={DeliveryDetail}/>
                    </Switch>
                </div>
            </Router>
    );
    }

    }

    render(
        <App/>
    , document.getElementById('app'));