"use strict";

import {Cell} from 'fixed-data-table-2';
import React from 'react';
import {Link} from 'react-router-dom';
import {
    FormControl
} from 'react-bootstrap';


class CollapseCell extends React.PureComponent {
    render() {
        const {data, rowIndex, columnKey, collapsedRows, callback, ...props} = this.props;
        return (
            <Cell {...props}>
                <a onClick={() => callback(rowIndex)}>
                    {collapsedRows.has(rowIndex) ? '\u25BC' : '\u25BA'}
                </a>
            </Cell>
        );
    }
};
module.exports.CollapseCell = CollapseCell;

class ColoredTextCell extends React.PureComponent {
    render() {
        const {data, rowIndex, columnKey, ...props} = this.props;
        return (
            <Cell {...props}>
                {this.colorizeText(data[rowIndex][columnKey], rowIndex)}
            </Cell>
        );
    }

    colorizeText(str, index) {
        let val, n = 0;
        return str.split('').map((letter) => {
            val = index * 70 + n++;
            let color = 'hsl(' + val + ', 100%, 50%)';
            return <span style={{color}} key={val}>{letter}</span>;
        });
    }
};
module.exports.ColoredTextCell = ColoredTextCell;

class DateCell extends React.PureComponent {
    render() {
        const {data, rowIndex, columnKey, ...props} = this.props;
        return (
            <Cell {...props}>
                {data[rowIndex][columnKey] == null ? null : new Date(data[rowIndex][columnKey]).toLocaleDateString("sk-SK")}
            </Cell>
        );
    }
};
module.exports.DateCell = DateCell;

class ImageCell extends React.PureComponent {
    render() {
        const {data, rowIndex, columnKey, ...props} = this.props;
        return (
            <ExampleImage
                src={data[rowIndex][columnKey]}
            />
        );
    }
};
module.exports.ImageCell = ImageCell;

class LinkCell extends React.PureComponent {
    render() {
        const {data, rowIndex, columnKey, idKey, ...props} = this.props;
        return (
            <Cell {...props}>
                <Link to={`order/${data[rowIndex][idKey]}`}>{data[rowIndex][columnKey]}</Link>
            </Cell>
        );
    }
};
module.exports.LinkCell = LinkCell;

class DeliveryLinkCell extends React.PureComponent {
    render() {
        const {data, rowIndex, columnKey, idKey, ...props} = this.props;
        return (
            <Cell {...props}>
                <Link to={`delivery/${data[rowIndex][idKey]}`}>{data[rowIndex][columnKey]}</Link>
            </Cell>
        );
    }
};
module.exports.DeliveryLinkCell = DeliveryLinkCell;

class PendingCell extends React.PureComponent {
    render() {
        const {data, rowIndex, columnKey, dataVersion, ...props} = this.props;
        const rowObject = data[rowIndex];
        return (
            <Cell {...props}>
                {rowObject ? rowObject[columnKey] : 'pending'}
            </Cell>
        );
    }
};
const PagedCell = ({data, ...props}) => {
    const dataVersion = data.getDataVersion();
    return (
        <PendingCell
            data={data}
            dataVersion={dataVersion}
            {...props}>
        </PendingCell>
    );
};
module.exports.PagedCell = PagedCell;

class RemovableHeaderCell extends React.PureComponent {
    render() {
        const {data, rowIndex, columnKey, callback, children, ...props} = this.props;
        return (
            <Cell {...props}>
                {children}
                <a style={{float: 'right'}} onClick={() => callback(columnKey)}>
                    {'\u274C'}
                </a>
            </Cell>
        );
    }
};
module.exports.RemovableHeaderCell = RemovableHeaderCell;

class TextCell extends React.PureComponent {
    render() {
        const {data, rowIndex, columnKey, ...props} = this.props;
        return (
            <Cell {...props}>
                {data[rowIndex][columnKey]}
            </Cell>
        );
    }
};
module.exports.TextCell = TextCell;

class CodeTextCell extends React.PureComponent {
    render() {
        const {data, rowIndex, columnKey, ...props} = this.props;
        return (
            <Cell {...props}>
                {data[rowIndex][columnKey].code}
            </Cell>
        );
    }
};
module.exports.CodeTextCell = CodeTextCell;

class AddressTextCell extends React.PureComponent {
    render() {
        const {data, rowIndex, columnKey, ...props} = this.props;
        return (
            <Cell {...props}>
                {data[rowIndex][columnKey.substring(2, 7)].address}
            </Cell>
        );
    }
};
module.exports.AddressTextCell = AddressTextCell;

class StreetTextCell extends React.PureComponent {
    render() {
        const {data, rowIndex, columnKey, ...props} = this.props;
        return (
            <Cell {...props}>
                {data[rowIndex][columnKey.substring(2, 7)].street}
            </Cell>
        );
    }
};
module.exports.StreetTextCell = StreetTextCell;

class MunicipalityTextCell extends React.PureComponent {
    render() {
        const {data, rowIndex, columnKey, ...props} = this.props;
        return (
            <Cell {...props}>
                {data[rowIndex][columnKey.substring(2, 7)].municipality}
            </Cell>
        );
    }
};
module.exports.MunicipalityTextCell = MunicipalityTextCell;

var SortTypes = {
    ASC: 'ASC',
    DESC: 'DESC',
}

function reverseSortDirection(sortDir) {
    return sortDir === SortTypes.DESC ? SortTypes.ASC : SortTypes.DESC;
}

class SortHeaderCell extends React.Component {
    constructor(props) {
        super(props);
    }

    _onSortChange = (event) => {
        event.preventDefault();

        if (this.props.onSortChange) {
            this.props.onSortChange(
                this.props.columnKey,
                this.props.sortDir ?
                    reverseSortDirection(this.props.sortDir) :
                    SortTypes.DESC
            );
        }
    }

    render() {
        var {onSortChange, sortDir, children, ...props} = this.props;
        return (
            <Cell {...props}>
                <a onClick={this._onSortChange}>
                    {children} {sortDir ? (sortDir === SortTypes.DESC ? '↓' : '↑') : ''}
                </a>
            </Cell>
        );
    }
};
module.exports.SortHeaderCell = SortHeaderCell;

class InputNumberCell extends React.Component {
    constructor(props) {
        super(props);
    }

    handleChangeInputValue = (e) => {
        const {rowIndex, columnKey, valueChnageHandler, ...props} = this.props
        valueChnageHandler(rowIndex, columnKey, e.target.value)
    }

    render() {
        const {data, rowIndex, columnKey, ...props} = this.props;
        return (
            <Cell {...props}>
                <FormControl type="number" onChange={this.handleChangeInputValue} value={data[rowIndex][columnKey]} />
            </Cell>
        );
    }
}
module.exports.InputNumberCell = InputNumberCell;

class InputTextCell extends React.Component {
    constructor(props) {
        super(props);
    }

    handleChangeInputValue = (e) => {
        const {rowIndex, columnKey, valueChnageHandler, ...props} = this.props
        valueChnageHandler(rowIndex, columnKey, e.target.value)
    }

    render() {
        const {data, rowIndex, columnKey, ...props} = this.props;
        return (
            <Cell {...props}>
                <FormControl type="text" onChange={this.handleChangeInputValue} value={data[rowIndex][columnKey]} />
            </Cell>
        );
    }
}
module.exports.InputTextCell = InputTextCell;
