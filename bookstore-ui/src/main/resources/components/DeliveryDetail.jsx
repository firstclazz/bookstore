import React from 'react';
import {withRouter} from 'react-router'
import {
    Col,
    ControlLabel,
    FormControl,
    FormGroup,
    Form,
    Navbar,
    Nav,
    NavItem,
    Button
} from 'react-bootstrap';
import { saveAs } from 'file-saver'
import TopMenu from './TopMenu.jsx';
import DeliveryItemsGrid from './DeliveryItemsGrid.jsx';
import getAPIPath from '../utils/FetchClient.js';

class DeliveryDetail extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            delivery: {
                order: {},
                deliveryItems: []
            }
        };
    }

    componentDidMount() {
        fetch(`${getAPIPath()}/delivery/${this.props.match.params.id}`, {
            method: "GET",
            headers: {
                "Accept": "application/json"
            },
            credentials: 'include'
        })
            .then((response) => {
                if (response.status == 401) {
                    this.props.history.push('/login')
                }
                return response.json();
            })
            .then((data) => {
                this.setState({delivery: data})
            })
            .catch(function(error) {
                console.log('error', error)
                throw error
            })
    }

    handlePrintDeliveryNote = () => {
        fetch(`${getAPIPath()}/deliveryNotePdf/${this.state.delivery.order.id}`, {
            method: "GET",
            headers: {
                "Accept": "application/pdf"
            },
            credentials: 'include',
            responseType: 'blob'
        })
            .then((response) => {
                if (response.status == 401) {
                    this.props.history.push('/login')
                }
                return response.blob();
            })
            .then(blob => saveAs(blob, `${this.state.delivery.deliveryNumber}.pdf`))
            .catch(function(error) {
                console.log('error', error)
                throw error
            })
    }

    handleSaveDeliveryNote = () => {
        // Create delivery note
        fetch(`${getAPIPath()}/delivery/${this.state.delivery.id}`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include',
            body: JSON.stringify(this.state.delivery)
        })
            .then((response) => {
                if (response.status == 401) {
                    this.props.history.push('/login')
                }
            })
            .catch(function (error) {
                console.log('error', error)
                throw error
            })
    }

    handleConfirmDeliveryNote = () => {
        // Create delivery note
        fetch(`${getAPIPath()}/confirmDelivery/${this.state.delivery.id}`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include',
            body: JSON.stringify(this.state.delivery)
        })
            .then((response) => {
                if (response.status == 401) {
                    this.props.history.push('/login')
                }
            })
            .catch(function (error) {
                console.log('error', error)
                throw error
            })
    }

    valueChangeHandler = (rowIndex, columnKey, value) => {
        this.state.delivery.deliveryItems[rowIndex][columnKey] = value
        this.setState({delivery: this.state.delivery})
    }

    render() {
        const {delivery} = this.state;

        return (
            <div>
                <TopMenu/>
                <Form horizontal>
                    {/* Code */}
                    <FormGroup bsSize="small" controlId="code">
                        <Col componentClass={ControlLabel} sm={2}>
                            Čislo dodacieho listu
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={delivery.deliveryNumber} />
                        </Col>
                    </FormGroup>

                    {/* Code */}
                    <FormGroup bsSize="small" controlId="code">
                        <Col componentClass={ControlLabel} sm={2}>
                            Kód objednávky
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={delivery.order.code} />
                        </Col>
                    </FormGroup>

                    {/* SchoolCode */}
                    <FormGroup bsSize="small" controlId="schoolCode">
                        <Col componentClass={ControlLabel} sm={2}>
                            IČO školy
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={delivery.order.schoolCode} />
                        </Col>
                    </FormGroup>

                    {/* Person */}
                    <FormGroup bsSize="small" controlId="person">
                        <Col componentClass={ControlLabel} sm={2}>
                            Kontaktná osoba
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={delivery.order.person} />
                        </Col>
                    </FormGroup>

                    {/* Year */}
                    <FormGroup bsSize="small" controlId="year">
                        <Col componentClass={ControlLabel} sm={2}>
                            Školský rok
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={delivery.order.year} />
                        </Col>
                    </FormGroup>

                    {/* Addressee */}
                    <FormGroup bsSize="small" controlId="address">
                        <Col componentClass={ControlLabel} sm={2}>
                            Adresa
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={delivery.order.address} />
                        </Col>
                    </FormGroup>

                    {/* Street */}
                    <FormGroup bsSize="small" controlId="street">
                        <Col componentClass={ControlLabel} sm={2}>
                            Ulica
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={delivery.order.street} />
                        </Col>
                    </FormGroup>

                    {/* PostalCode */}
                    <FormGroup bsSize="small" controlId="code">
                        <Col componentClass={ControlLabel} sm={2}>
                            PSČ
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={delivery.order.postalCode} />
                        </Col>
                    </FormGroup>

                    {/* Municipality */}
                    <FormGroup bsSize="small" controlId="municipality">
                        <Col componentClass={ControlLabel} sm={2}>
                            Mesto
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={delivery.order.municipality} />
                        </Col>
                    </FormGroup>

                    {/* District */}
                    <FormGroup bsSize="small" controlId="district">
                        <Col componentClass={ControlLabel} sm={2}>
                            Okres
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={delivery.order.district} />
                        </Col>
                    </FormGroup>

                    {/* Tel */}
                    <FormGroup bsSize="small" controlId="tel">
                        <Col componentClass={ControlLabel} sm={2}>
                            Telefónne číslo
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={delivery.order.phone} />
                        </Col>
                    </FormGroup>

                    {/* Email */}
                    <FormGroup bsSize="small" controlId="email">
                        <Col componentClass={ControlLabel} sm={2}>
                            Email
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={delivery.order.email} />
                        </Col>
                    </FormGroup>

                    {/* Created */}
                    <FormGroup bsSize="small" controlId="created">
                        <Col componentClass={ControlLabel} sm={2}>
                            Dátum vytvorenia
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={new Date(delivery.order.created).toLocaleDateString("sk-SK")} />
                        </Col>
                    </FormGroup>

                    {/* Confirmed */}
                    <FormGroup bsSize="small" controlId="confirmed">
                        <Col componentClass={ControlLabel} sm={2}>
                            Dátum schválenia
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={delivery.order.confirmed == null ? null : new Date(delivery.order.confirmed).toLocaleDateString("sk-SK")} />
                        </Col>
                    </FormGroup>

                    {/* Revision */}
                    <FormGroup bsSize="small" controlId="revision">
                        <Col componentClass={ControlLabel} sm={2}>
                            Revízia
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={delivery.order.revision} />
                        </Col>
                    </FormGroup>

                    {/* Status */}
                    <FormGroup bsSize="small" controlId="statusName">
                        <Col componentClass={ControlLabel} sm={2}>
                            Stav objednávky
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={delivery.order.statusName} />
                        </Col>
                    </FormGroup>

                </Form>

                <DeliveryItemsGrid data={delivery.deliveryItems} valueChangeHandler={this.valueChangeHandler}/>

                <Navbar fixedBottom>
                    <Nav pullRight>
                        <NavItem>
                            <Button bsStyle="primary" disabled={!!delivery.confirmed} onClick={this.handleSaveDeliveryNote}>Uložiť dodací list</Button>
                        </NavItem>
                        <NavItem>
                            <Button bsStyle="primary" disabled={!!delivery.confirmed} onClick={this.handleConfirmDeliveryNote}>Potvrdiť dodací list</Button>
                        </NavItem>
                        <NavItem>
                            <Button bsStyle="primary" onClick={this.handlePrintDeliveryNote}>Vylačiť dodací list</Button>
                        </NavItem>
                    </Nav>
                </Navbar>

            </div>
        );
    }

}

export default withRouter(DeliveryDetail);