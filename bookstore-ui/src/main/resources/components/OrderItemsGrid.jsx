import React from 'react';
import Dimensions from 'react-dimensions';
import {withRouter} from 'react-router'
import {Table, Column, Cell} from 'fixed-data-table-2';
import {TextCell} from './helpers/GridCells.jsx';

class OrderItemsGrid extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            itemsData: [],
            orderId: ""
        }
    }

    render() {
        const {data, ...props} = this.props;
        this.state.itemsData = data;
        if (this.state.itemsData === undefined || this.state.orderId === undefined) {
            return null;
        }
        return (
            <div>
                <Table
                    showScrollbarX={false}
                    showScrollbarY={false}
                    rowsCount={this.state.itemsData.length}
                    rowHeight={50}
                    headerHeight={50}
                    touchScrollEnabled={true}
                    width={this.props.containerWidth}
                    height={(this.state.itemsData.length + 1) * 50}>
                    <Column
                        header={<Cell>EAN kód</Cell>}
                        columnKey="bookEan"
                        cell={<TextCell data={this.state.itemsData}/>}
                        width={200}
                    />
                    <Column
                        header={<Cell>ISBN kód</Cell>}
                        columnKey="bookIsbn"
                        cell={<TextCell data={this.state.itemsData}/>}
                        width={200}
                    />
                    <Column
                        header={<Cell>Názov knihy</Cell>}
                        columnKey="bookName"
                        cell={<TextCell data={this.state.itemsData}/>}
                        width={this.props.containerWidth - 550 < 150 ? 150 : this.props.containerWidth - 550}
                    />
                    <Column
                        header={<Cell>Množstvo</Cell>}
                        columnKey="amount"
                        cell={<TextCell data={this.state.itemsData}/>}
                        width={150}
                    />
                </Table>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
            </div>
        );
    }
}

export default withRouter(Dimensions()(OrderItemsGrid));
