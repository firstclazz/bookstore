import React from 'react';
import {withRouter} from 'react-router'
import TopMenu from './TopMenu.jsx'
import {Table, Column, Cell} from 'fixed-data-table-2';
import getAPIPath from '../utils/FetchClient.js';
import Dimensions from 'react-dimensions';
import { saveAs } from 'file-saver';
import {
    TextCell,
    DeliveryLinkCell,
    CodeTextCell,
    AddressTextCell,
    StreetTextCell,
    MunicipalityTextCell
} from './helpers/GridCells.jsx';
import {
    Pagination,
    Navbar,
    Nav,
    NavItem,
    Button
} from 'react-bootstrap';

var pageOffset = 250;
var minTableHeight = 300;

function integer(x) {
    return x - x % 1;
}

class DeliveriesGrid extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            deliveryData: [],
            activePage: 1,
            pageCount: 0,
            isPdfLoading: false,
            isXlsLoading: false
        };
    }

    componentDidMount() {
        this.fetchDataSize()
        this.fetchData()
    }

    fetchDataSize = () => {
        var pageSize = integer((this.props.containerHeight - pageOffset < minTableHeight ? minTableHeight : this.props.containerHeight - pageOffset) / 50);
        var url = "/deliveryCount"
        fetch(getAPIPath() + url, {
            method: "GET",
            headers: {
                "Accept": "application/json"
            },
            credentials: 'include'
        })
            .then((response) => {
                if (response.status == 401) {
                    this.props.history.push('/login')
                }
                return response.json();
            })
            .then((data) => {
                this.setState({pageCount: data / pageSize})
            })
            .catch(function(error) {
                console.log('error', error)
                throw error
            })
    }

    fetchData = () => {
        var pageSize = integer((this.props.containerHeight - pageOffset < minTableHeight ? minTableHeight : this.props.containerHeight - pageOffset) / 50);

        // Fetch order
        var url = "/deliveries?page=" + this.state.activePage + "&pageSize=" + integer(pageSize)
        fetch(getAPIPath() + url, {
            method: "GET",
            headers: {
                "Accept": "application/json"
            },
            credentials: 'include'
        })
            .then((response) => {
                if (response.status == 401) {
                    this.props.history.push('/login')
                }
                return response.json();
            })
            .then((data) => {
                this.setState({deliveryData: data})
            })
            .catch(function(error) {
                console.log('error', error)
                throw error
            })
    }

    handlePrintDeliveryGrid = () => {
        this.setState({isPdfLoading: true});
        // Fetch order
        var url = "/deliveryNotesPdf"
        fetch(getAPIPath() + url, {
            method: "GET",
            headers: {
                "Accept": "application/pdf"
            },
            credentials: 'include',
            responseType: 'blob'
        })
            .then((response) => {
                this.setState({isPdfLoading: false});
                if (response.status == 401) {
                    this.props.history.push('/login')
                }
                return response.blob();
            })
            .then(blob => saveAs(blob, "delivery_export.pdf"))
            .catch(function(error) {
                this.setState({isPdfLoading: false});
                console.log('error', error)
                throw error
            })
    }

    handleExportDeliveryGrid = () => {
        this.setState({isXlsLoading: true});

        // Fetch order
        var url = "/deliveryNotesXls"
        fetch(getAPIPath() + url, {
            method: "GET",
            headers: {
                "Accept": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            },
            credentials: 'include',
            responseType: 'blob'
        })
            .then((response) => {
                this.setState({isXlsLoading: false});
                if (response.status == 401) {
                    this.props.history.push('/login')
                }
                return response.blob();
            })
            .then(blob => saveAs(blob, "delivery_export.xlsx"))
            .catch(function(error) {
                this.setState({isXlsLoading: false});
                console.log('error', error)
                throw error
            })
    }

    handlePagination = (eventKey) => {
        this.setState({
            activePage: eventKey
        });
        // Fetch data
        this.state.activePage = eventKey;
        this.fetchData();
    }

    render() {
        let isPdfLoading = this.state.isPdfLoading;
        let isXlsLoading = this.state.isXlsLoading;
        return (
            <div>
                <TopMenu/>
                <Table
                    rowsCount={this.state.deliveryData.length}
                    rowHeight={50}
                    headerHeight={50}
                    touchScrollEnabled={true}
                    width={this.props.containerWidth}
                    height={this.props.containerHeight - pageOffset < minTableHeight ? minTableHeight : this.props.containerHeight - pageOffset}>
                    <Column
                        header={<Cell>Číslo dodacieho listu</Cell>}
                        columnKey="deliveryNumber"
                        cell={<DeliveryLinkCell data={this.state.deliveryData} idKey="id"/>}
                        width={180}
                    />
                    <Column
                        header={<Cell>Kód objednávky</Cell>}
                        columnKey="order"
                        cell={<CodeTextCell data={this.state.deliveryData}/>}
                        width={150}
                    />
                    <Column
                        header={<Cell>Adresa</Cell>}
                        columnKey="1_order"
                        cell={<AddressTextCell data={this.state.deliveryData}/>}
                        width={this.props.containerWidth - 760 - 18 < 150 ? 150 : this.props.containerWidth - 760 - 18}
                    />
                    <Column
                        header={<Cell>Ulica</Cell>}
                        columnKey="2_order"
                        cell={<StreetTextCell data={this.state.deliveryData}/>}
                        width={200}
                    />
                    <Column
                        header={<Cell>Mesto</Cell>}
                        columnKey="3_order"
                        cell={<MunicipalityTextCell data={this.state.deliveryData}/>}
                        width={150}
                    />
                    <Column
                        header={<Cell>Položiek</Cell>}
                        columnKey="deliveryItemsCount"
                        cell={<TextCell data={this.state.deliveryData}/>}
                        width={80}
                    />
                </Table>
                <div className="text-center">
                    <Pagination
                        prev
                        next
                        first
                        last
                        ellipsis
                        boundaryLinks
                        items={Math.ceil(this.state.pageCount)}
                        maxButtons={5}
                        activePage={this.state.activePage}
                        onSelect={this.handlePagination}/>
                </div>

                <Navbar>
                    <Nav pullRight>
                        <NavItem>
                            <Button bsStyle="primary"
                                    disabled={isPdfLoading}
                                    onClick={!isPdfLoading ? this.handlePrintDeliveryGrid : null}>
                                {isPdfLoading ? 'Generujem...' : 'Tlač zoznamu'}
                            </Button>
                        </NavItem>
                        <NavItem>
                            <Button bsStyle="primary"
                                    disabled={isXlsLoading}
                                    onClick={!isXlsLoading ? this.handleExportDeliveryGrid : null}>
                                {isXlsLoading ? 'Generujem...' : 'Export do XLS'}
                            </Button>
                        </NavItem>
                        <NavItem>
                        </NavItem>
                    </Nav>
                </Navbar>

            </div>
        )
    }
}
export default withRouter(Dimensions()(DeliveriesGrid));