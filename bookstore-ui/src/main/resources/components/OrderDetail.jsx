import React from 'react';
import {withRouter} from 'react-router'
import {
    Col,
    ControlLabel,
    FormControl,
    FormGroup,
    Form,
    Navbar,
    Nav,
    NavItem,
    Button
} from 'react-bootstrap';
import TopMenu from './TopMenu.jsx';
import OrderItemsGrid from './OrderItemsGrid.jsx';
import getAPIPath from '../utils/FetchClient.js';

class OrderDetail extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            order: {}
        };
    }

    componentDidMount() {
        fetch(`${getAPIPath()}/order/${this.props.match.params.id}`, {
            method: "GET",
            headers: {
                "Accept": "application/json"
            },
            credentials: 'include'
        })
            .then((response) => {
                if (response.status == 401) {
                    this.props.history.push('/login')
                }
                return response.json();
            })
            .then((data) => {
                this.setState({order: data})
            })
            .catch(function(error) {
                console.log('error', error)
                throw error
            })
    }

    handleCreateDeliveryNote = () => {
        // Create delivery note
        fetch(`${getAPIPath()}/delivery/${this.state.order.id}`, {
            method: "PUT",
            headers: {
                "Accept": "application/json"
            },
            credentials: 'include'
        })
            .then((response) => {
                if (response.status == 401) {
                    this.props.history.push('/login')
                }
                this.props.history.push(`/delivery/${this.state.order.id}`)
            })
            .catch(function (error) {
                console.log('error', error)
                throw error
            })
    }

    render() {
        const {order} = this.state;
        return (
            <div>
                <TopMenu/>
                <Form horizontal>
                    {/* Code */}
                    <FormGroup bsSize="small" controlId="code">
                        <Col componentClass={ControlLabel} sm={2}>
                            Kód objednávky
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={order.code} />
                        </Col>
                    </FormGroup>

                    {/* SchoolCode */}
                    <FormGroup bsSize="small" controlId="schoolCode">
                        <Col componentClass={ControlLabel} sm={2}>
                            IČO školy
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={order.schoolCode} />
                        </Col>
                    </FormGroup>

                    {/* Person */}
                    <FormGroup bsSize="small" controlId="person">
                        <Col componentClass={ControlLabel} sm={2}>
                            Kontaktná osoba
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={order.person} />
                        </Col>
                    </FormGroup>

                    {/* Year */}
                    <FormGroup bsSize="small" controlId="year">
                        <Col componentClass={ControlLabel} sm={2}>
                            Školský rok
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={order.year} />
                        </Col>
                    </FormGroup>

                    {/* Addressee */}
                    <FormGroup bsSize="small" controlId="address">
                        <Col componentClass={ControlLabel} sm={2}>
                            Adresa
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={order.address} />
                        </Col>
                    </FormGroup>

                    {/* Street */}
                    <FormGroup bsSize="small" controlId="street">
                        <Col componentClass={ControlLabel} sm={2}>
                            Ulica
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={order.street} />
                        </Col>
                    </FormGroup>

                    {/* PostalCode */}
                    <FormGroup bsSize="small" controlId="code">
                        <Col componentClass={ControlLabel} sm={2}>
                            PSČ
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={order.postalCode} />
                        </Col>
                    </FormGroup>

                    {/* Municipality */}
                    <FormGroup bsSize="small" controlId="municipality">
                        <Col componentClass={ControlLabel} sm={2}>
                            Mesto
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={order.municipality} />
                        </Col>
                    </FormGroup>

                    {/* District */}
                    <FormGroup bsSize="small" controlId="district">
                        <Col componentClass={ControlLabel} sm={2}>
                            Okres
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={order.district} />
                        </Col>
                    </FormGroup>

                    {/* Tel */}
                    <FormGroup bsSize="small" controlId="tel">
                        <Col componentClass={ControlLabel} sm={2}>
                            Telefónne číslo
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={order.phone} />
                        </Col>
                    </FormGroup>

                    {/* Email */}
                    <FormGroup bsSize="small" controlId="email">
                        <Col componentClass={ControlLabel} sm={2}>
                            Email
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={order.email} />
                        </Col>
                    </FormGroup>

                    {/* Created */}
                    <FormGroup bsSize="small" controlId="created">
                        <Col componentClass={ControlLabel} sm={2}>
                            Dátum vytvorenia
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={new Date(order.created).toLocaleDateString("sk-SK")} />
                        </Col>
                    </FormGroup>

                    {/* Confirmed */}
                    <FormGroup bsSize="small" controlId="confirmed">
                        <Col componentClass={ControlLabel} sm={2}>
                            Dátum schválenia
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={order.confirmed == null ? null : new Date(order.confirmed).toLocaleDateString("sk-SK")} />
                        </Col>
                    </FormGroup>

                    {/* Revision */}
                    <FormGroup bsSize="small" controlId="revision">
                        <Col componentClass={ControlLabel} sm={2}>
                            Revízia
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={order.revision} />
                        </Col>
                    </FormGroup>

                    {/* Status */}
                    <FormGroup bsSize="small" controlId="statusName">
                        <Col componentClass={ControlLabel} sm={2}>
                            Stav objednávky
                        </Col>
                        <Col sm={10}>
                            <FormControl type="input" disabled="true" value={order.statusName} />
                        </Col>
                    </FormGroup>

                </Form>

                <OrderItemsGrid data={order.orderItems} />

                <Navbar fixedBottom>
                    <Nav pullRight>
                        <NavItem>
                            <Button bsStyle="primary" onClick={this.handleCreateDeliveryNote}>{order.deliveryCreated ? 'Otvoriť dodací list' : 'Vytvoriť dodací list'}</Button>
                        </NavItem>
                    </Nav>
                </Navbar>

            </div>
        );
    }

}

export default withRouter(OrderDetail);;