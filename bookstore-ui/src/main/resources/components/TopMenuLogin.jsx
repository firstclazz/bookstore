import React from 'react';
import {withRouter} from 'react-router-dom'
import {
    Navbar,
    Nav,
    NavItem,
    NavDropdown,
    MenuItem
} from 'react-bootstrap';

class TopMenuLogin extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Navbar>
                    <Navbar.Header>
                        <Navbar.Brand>
                            DISTRIBUČNÝ SKLAD
                        </Navbar.Brand>
                    </Navbar.Header>
                </Navbar>
            </div>
        );
    }

}

export default withRouter(TopMenuLogin);