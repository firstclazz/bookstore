import React from 'react';
import {withRouter} from 'react-router-dom'
import {Button,
    ControlLabel,
    Form,
    FormControl,
    FormGroup,
    Col,
    Panel
} from 'react-bootstrap';
import TopMenuLogin from './TopMenuLogin.jsx';
import getAPIPath from "../utils/FetchClient.js";

class Login extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: ""
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleSubmit(event) {
        event.preventDefault();
        fetch(getAPIPath() + "/login", {
            method: "POST",
            headers: {"Content-Type": "application/x-www-form-urlencoded"},
            body: `username=${this.state.username}&password=${this.state.password}`,
            credentials: 'include'
        })
            .then((response) => {
                if (response.status == 401) {
                    this.setState({
                        errorMessage: "Zlé meno alebo heslo!",
                        password: ""
                    })
                }
                if (response.ok) {
                    this.props.history.push('/')
                }
            })
            .catch(function (error) {
                console.log('error', error)
                throw error
            })
    }

    render() {
        const {errorMessage} = this.props

        return (
            <div>
                <TopMenuLogin/>
                <Panel height={200}>
                    <Form horizontal>
                        <FormGroup bsSize="small" controlId="Username">
                            <Col componentClass={ControlLabel} sm={4}>
                                Používateľské meno
                            </Col>
                            <Col sm={4}>
                                <FormControl
                                    type="username"
                                    name="username"
                                    value={this.state.username}
                                    onChange={this.handleInputChange}/>
                            </Col>
                        </FormGroup>
                        <FormGroup bsSize="small" controlId="Password">
                            <Col componentClass={ControlLabel} sm={4}>
                                Heslo
                            </Col>
                            <Col sm={4}>
                                <FormControl
                                    type="password"
                                    name="password"
                                    value={this.state.password}
                                    onChange={this.handleInputChange}/>
                            </Col>
                        </FormGroup>
                        <div className="center block">
                            <Button
                                type="submit"
                                bsStyle="primary"
                                className="center-block"
                                onClick={this.handleSubmit}>Prihlásiť</Button>
                        </div>
                        {errorMessage &&
                        <p style={{color: 'red'}}>{errorMessage}</p>
                        }
                    </Form>
                </Panel>
            </div>
        )
    }
}

export default withRouter(Login);