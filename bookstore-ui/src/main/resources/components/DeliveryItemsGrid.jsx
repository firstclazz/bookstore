import React from 'react';
import Dimensions from 'react-dimensions';
import {withRouter} from 'react-router'
import {Table, Column, Cell} from 'fixed-data-table-2';
import {TextCell, InputNumberCell, InputTextCell} from './helpers/GridCells.jsx';

class ItemsGrid extends React.Component {

    constructor(props) {
        super(props);
    }

    _valueChangeHandler = (rowIndex, columnKey, value) => {
        const {valueChangeHandler, ...props} = this.props;
        valueChangeHandler(rowIndex, columnKey, value)
    }

    render() {
        const {data, ...props} = this.props;
        return (
           <div>
                <Table
                    showScrollbarX={false}
                    showScrollbarY={false}
                    rowsCount={data.length}
                    rowHeight={50}
                    headerHeight={50}
                    touchScrollEnabled={true}
                    width={this.props.containerWidth}
                    height={(data.length + 1) * 50}>
                    <Column
                        header={<Cell>EAN kód</Cell>}
                        columnKey="bookEan"
                        cell={<TextCell data={data}/>}
                        width={200}
                    />
                    <Column
                        header={<Cell>ISBN kód</Cell>}
                        columnKey="bookIsbn"
                        cell={<InputTextCell data={data} valueChnageHandler={this._valueChangeHandler}/>}
                        width={150}
                    />
                    <Column
                        header={<Cell>Názov knihy</Cell>}
                        columnKey="bookName"
                        cell={<TextCell data={data}/>}
                        width={this.props.containerWidth - 650 < 150 ? 150 : this.props.containerWidth - 650}
                    />
                    <Column
                        header={<Cell>Objednané množstvo</Cell>}
                        columnKey="orderedAmount"
                        cell={<TextCell data={data}/>}
                        width={150}
                    />
                    <Column
                        header={<Cell>Dodané množstvo</Cell>}
                        columnKey="amount"
                        cell={<InputNumberCell data={data} valueChnageHandler={this._valueChangeHandler}/>}
                        width={150}
                    />
                </Table>
               <div>&nbsp;</div>
               <div>&nbsp;</div>
               <div>&nbsp;</div>
               <div>&nbsp;</div>
               <div>&nbsp;</div>
               <div>&nbsp;</div>
               <div>&nbsp;</div>
               <div>&nbsp;</div>
               <div>&nbsp;</div>
           </div>
        );
    }
}

export default withRouter(Dimensions()(ItemsGrid));
