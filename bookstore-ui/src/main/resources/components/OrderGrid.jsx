import React from 'react';
import Dimensions from 'react-dimensions';
import {withRouter} from 'react-router'
import {Table, Column, Cell} from 'fixed-data-table-2';
import {
    Pagination,
    FormControl,
    Form,
    FormGroup,
    Button,
    Navbar,
    Nav,
    NavItem
} from 'react-bootstrap';
import { saveAs } from 'file-saver';
import TopMenu from './TopMenu.jsx';
import {TextCell, LinkCell, SortHeaderCell, DateCell} from './helpers/GridCells.jsx';
import getAPIPath from '../utils/FetchClient.js';

var pageOffset = 250;
var minTableHeight = 300;

function integer(x) {
    return x - x % 1;
}

class OrderGrid extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            ordersData: [],
            activePage: 1,
            pageCount: 0,
            colSortDirs: {},
            isPdfLoading: false,
            isXlsLoading: false,
            filter: {
                status: '717660003',
                code: undefined,
                address: undefined,
                street: undefined,
                municipality: undefined
            }
        };
    }

    componentDidMount() {
        this.fetchDataSize()
        this.fetchData()
    }

    fetchDataSize = () => {
        var pageSize = integer((this.props.containerHeight - pageOffset < minTableHeight ? minTableHeight : this.props.containerHeight - pageOffset) / 50);

        // Fetch orders count

        var url = "/ordersCount?q=1"
        // Filter
        if (this.state.filter.status !== undefined) {
            url += `&status=${this.state.filter.status}`
        }
        if (this.state.filter.code !== undefined) {
            url += `&code=${this.state.filter.code}`
        }
        if (this.state.filter.address !== undefined) {
            url += `&address=${this.state.filter.address}`
        }
        if (this.state.filter.street !== undefined) {
            url += `&street=${this.state.filter.street}`
        }
        if (this.state.filter.municipality !== undefined) {
            url += `&municipality=${this.state.filter.municipality}`
        }

        fetch(getAPIPath() + url, {
            method: "GET",
            headers: {
                "Accept": "application/json"
            },
            credentials: 'include'
        })
            .then((response) => {
                if (response.status == 401) {
                    this.props.history.push('/login')
                }
                return response.json();
            })
            .then((data) => {
                this.setState({pageCount: data / pageSize})
            })
            .catch(function(error) {
                console.log('error', error)
                throw error
            })
    }

    fetchData = () => {

        var pageSize = integer((this.props.containerHeight - pageOffset < minTableHeight ? minTableHeight : this.props.containerHeight - pageOffset) / 50);

        // Fetch order
        var url = "/orders?page=" + this.state.activePage + "&pageSize=" + integer(pageSize)

        // Order
        if(this.state.colSortDirs.code) {
            url += `&sortColumn=code&sortDir=${this.state.colSortDirs.code}`
        } else if(this.state.colSortDirs.address) {
            url += `&sortColumn=address&sortDir=${this.state.colSortDirs.address}`
        } else if(this.state.colSortDirs.street) {
            url += `&sortColumn=street&sortDir=${this.state.colSortDirs.street}`
        } else if(this.state.colSortDirs.municipality) {
            url += `&sortColumn=municipality&sortDir=${this.state.colSortDirs.municipality}`
        } else if(this.state.colSortDirs.created) {
            url += `&sortColumn=created&sortDir=${this.state.colSortDirs.created}`
        } else if(this.state.colSortDirs.statusName) {
            url += `&sortColumn=status&sortDir=${this.state.colSortDirs.statusName}`
        }

        // Filter
        if (this.state.filter.status !== undefined) {
            url += `&status=${this.state.filter.status}`
        }
        if (this.state.filter.code !== undefined) {
            url += `&code=${this.state.filter.code}`
        }
        if (this.state.filter.address !== undefined) {
            url += `&address=${this.state.filter.address}`
        }
        if (this.state.filter.street !== undefined) {
            url += `&street=${this.state.filter.street}`
        }
        if (this.state.filter.municipality !== undefined) {
            url += `&municipality=${this.state.filter.municipality}`
        }

        fetch(getAPIPath() + url, {
            method: "GET",
            headers: {
                "Accept": "application/json"
            },
            credentials: 'include'
        })
            .then((response) => {
                if (response.status == 401) {
                    this.props.history.push('/login')
                }
                return response.json();
            })
            .then((data) => {
                this.setState({ordersData: data})
            })
            .catch(function(error) {
                console.log('error', error)
                throw error
            })
    }

    handlePagination = (eventKey) => {
        this.setState({
            activePage: eventKey
        });
        // Fetch data
        this.state.activePage = eventKey;
        this.fetchData();
    }

    _onSortChange = (columnKey, sortDir) => {
        this.setState({
            colSortDirs: {
                [columnKey]: sortDir
            },
        });
        this.state.colSortDirs = {
            [columnKey]: sortDir
        }
        this.fetchData();
    }

    handleSearch = (e) => {
        event.preventDefault()
        this.fetchDataSize()
        this.fetchData()
    }

    handleStatusChange = (e) => {
        var filter = this.state.filter
        filter.status = e.target.value == "ALL" ? undefined : e.target.value
        this.setState({filter});
    }

    handleCodeChange = (e) => {
        var filter = this.state.filter
        filter.code = e.target.value
        this.setState({filter});
    }

    handleAddressChange = (e) => {
        var filter = this.state.filter
        filter.address = e.target.value
        this.setState({filter});
    }

    handleStreetChange = (e) => {
        var filter = this.state.filter
        filter.street = e.target.value
        this.setState({filter});
    }

    handleMunicipalityChange = (e) => {
        var filter = this.state.filter
        filter.municipality = e.target.value
        this.setState({filter});
    }

    handlePrintOderGrid = () => {
        this.setState({isPdfLoading: true});

        // Fetch order
        var url = "/ordersPdf?q=1"

        // Order
        if(this.state.colSortDirs.code) {
            url += `&sortColumn=code&sortDir=${this.state.colSortDirs.code}`
        } else if(this.state.colSortDirs.address) {
            url += `&sortColumn=address&sortDir=${this.state.colSortDirs.address}`
        } else if(this.state.colSortDirs.street) {
            url += `&sortColumn=street&sortDir=${this.state.colSortDirs.street}`
        } else if(this.state.colSortDirs.municipality) {
            url += `&sortColumn=municipality&sortDir=${this.state.colSortDirs.municipality}`
        } else if(this.state.colSortDirs.created) {
            url += `&sortColumn=created&sortDir=${this.state.colSortDirs.created}`
        } else if(this.state.colSortDirs.statusName) {
            url += `&sortColumn=status&sortDir=${this.state.colSortDirs.statusName}`
        }

        // Filter
        if (this.state.filter.status !== undefined) {
            url += `&status=${this.state.filter.status}`
        }
        if (this.state.filter.code !== undefined) {
            url += `&code=${this.state.filter.code}`
        }
        if (this.state.filter.address !== undefined) {
            url += `&address=${this.state.filter.address}`
        }
        if (this.state.filter.street !== undefined) {
            url += `&street=${this.state.filter.street}`
        }
        if (this.state.filter.municipality !== undefined) {
            url += `&municipality=${this.state.filter.municipality}`
        }

        fetch(getAPIPath() + url, {
            method: "GET",
            headers: {
                "Accept": "application/pdf"
            },
            credentials: 'include',
            responseType: 'blob'
        })
            .then((response) => {
                this.setState({isPdfLoading: false});
                if (response.status == 401) {
                    this.props.history.push('/login')
                }
                return response.blob();
            })
            .then(blob => saveAs(blob, "order_export.pdf"))
            .catch(function(error) {
                this.setState({isPdfLoading: false});
                console.log('error', error)
                throw error
            })
    }

    handleExportOderGrid = () => {
        this.setState({isXlsLoading: true});

        // Fetch order
        var url = "/ordersXls?q=1"

        // Order
        if(this.state.colSortDirs.code) {
            url += `&sortColumn=code&sortDir=${this.state.colSortDirs.code}`
        } else if(this.state.colSortDirs.address) {
            url += `&sortColumn=address&sortDir=${this.state.colSortDirs.address}`
        } else if(this.state.colSortDirs.street) {
            url += `&sortColumn=street&sortDir=${this.state.colSortDirs.street}`
        } else if(this.state.colSortDirs.municipality) {
            url += `&sortColumn=municipality&sortDir=${this.state.colSortDirs.municipality}`
        } else if(this.state.colSortDirs.created) {
            url += `&sortColumn=created&sortDir=${this.state.colSortDirs.created}`
        } else if(this.state.colSortDirs.statusName) {
            url += `&sortColumn=status&sortDir=${this.state.colSortDirs.statusName}`
        }

        // Filter
        if (this.state.filter.status !== undefined) {
            url += `&status=${this.state.filter.status}`
        }
        if (this.state.filter.code !== undefined) {
            url += `&code=${this.state.filter.code}`
        }
        if (this.state.filter.address !== undefined) {
            url += `&address=${this.state.filter.address}`
        }
        if (this.state.filter.street !== undefined) {
            url += `&street=${this.state.filter.street}`
        }
        if (this.state.filter.municipality !== undefined) {
            url += `&municipality=${this.state.filter.municipality}`
        }

        fetch(getAPIPath() + url, {
            method: "GET",
            headers: {
                "Accept": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            },
            credentials: 'include',
            responseType: 'blob'
        })
            .then((response) => {
                this.setState({isXlsLoading: false});
                if (response.status == 401) {
                    this.props.history.push('/login')
                }
                return response.blob();
            })
            .then(blob => saveAs(blob, "order_export.xlsx"))
            .catch(function(error) {
                this.setState({isXlsLoading: false});
                console.log('error', error)
                throw error
            })
    }

    render() {
        var {colSortDirs} = this.state;
        let isPdfLoading = this.state.isPdfLoading;
        let isXlsLoading = this.state.isXlsLoading;
        return (
            <div>
                <TopMenu/>

                <Form inline onSubmit={this.handleSearch.bind(this)}>

                    <Navbar>
                        <Navbar.Header>
                            <Navbar.Brand>
                                Filter
                            </Navbar.Brand>
                        </Navbar.Header>
                        <Nav>
                                <Navbar.Form>
                                    <FormGroup>
                                        {/* Status */}
                                        <FormControl bsSize="small" componentClass="select" value={this.state.filter.status} onChange={this.handleStatusChange}>
                                            <option value="ALL">Všetky</option>
                                            <option value={2}>Zrušená</option>
                                            <option value={717660003}>Na distribúciu</option>
                                            <option value={717660004}>Č̌iastočne dodaná</option>
                                            <option value={717660007}>Úplne dodaná</option>
                                            <option value={717660008}>Presunutá</option>
                                        </FormControl>
                                    </FormGroup>
                                    {' '}
                                    <FormGroup>
                                        {/* Code */}
                                        <FormControl placeholder="Kód objednávky" bsSize="small" type="input" value={this.state.filter.code} onChange={this.handleCodeChange}/>
                                    </FormGroup>
                                    {' '}
                                    <FormGroup>
                                        {/*Address*/}
                                        <FormControl placeholder="Adresa" bsSize="small" type="input" value={this.state.filter.address} onChange={this.handleAddressChange}/>
                                    </FormGroup>
                                    {' '}
                                    <FormGroup>
                                        {/*Street*/}
                                        <FormControl placeholder="Ulica" bsSize="small" type="input" value={this.state.filter.street} onChange={this.handleStreetChange}/>
                                    </FormGroup>
                                    {' '}
                                    <FormGroup>
                                        {/*Municipality*/}
                                        <FormControl placeholder="Mesto" bsSize="small" type="input" value={this.state.filter.municipality} onChange={this.handleMunicipalityChange}/>
                                    </FormGroup>
                                    {' '}
                                    <Button bsSize="small" type="submit">Hľadaj</Button>

                                </Navbar.Form>
                        </Nav>
                    </Navbar>
                </Form>

                <Table
                    rowsCount={this.state.ordersData.length}
                    rowHeight={50}
                    headerHeight={50}
                    touchScrollEnabled={true}
                    width={this.props.containerWidth}
                    height={this.props.containerHeight - pageOffset < minTableHeight ? minTableHeight : this.props.containerHeight - pageOffset}>
                    <Column
                        header={<SortHeaderCell onSortChange={this._onSortChange} sortDir={colSortDirs.code}>Kód</SortHeaderCell>}
                        columnKey="code"
                        cell={<LinkCell data={this.state.ordersData} idKey="id"/>}
                        width={150}
                    />
                    <Column
                        header={<SortHeaderCell onSortChange={this._onSortChange} sortDir={colSortDirs.address}>Adresa</SortHeaderCell>}
                        columnKey="address"
                        cell={<TextCell data={this.state.ordersData}/>}
                        width={this.props.containerWidth - 860 - 18 < 150 ? 150 : this.props.containerWidth - 860 - 18}
                    />
                    <Column
                        header={<SortHeaderCell onSortChange={this._onSortChange} sortDir={colSortDirs.street}>Ulica</SortHeaderCell>}
                        columnKey="street"
                        cell={<TextCell data={this.state.ordersData}/>}
                        width={200}
                    />
                    <Column
                        header={<SortHeaderCell onSortChange={this._onSortChange} sortDir={colSortDirs.municipality}>Mesto</SortHeaderCell>}
                        columnKey="municipality"
                        cell={<TextCell data={this.state.ordersData}/>}
                        width={200}
                    />
                    <Column
                        header={<SortHeaderCell onSortChange={this._onSortChange} sortDir={colSortDirs.created}>Vytvorená</SortHeaderCell>}
                        columnKey="created"
                        cell={<DateCell data={this.state.ordersData}/>}
                        width={100}
                    />
                    <Column
                        header={<SortHeaderCell onSortChange={this._onSortChange} sortDir={colSortDirs.statusName}>Stav</SortHeaderCell>}
                        columnKey="statusName"
                        cell={<TextCell data={this.state.ordersData}/>}
                        width={130}
                    />
                    <Column
                        header={<Cell>Položiek</Cell>}
                        columnKey="orderItemsCount"
                        cell={<TextCell data={this.state.ordersData}/>}
                        width={80}
                    />
                </Table>
                <div className="text-center">
                    <Pagination
                            prev
                            next
                            first
                            last
                            ellipsis
                            boundaryLinks
                            items={Math.ceil(this.state.pageCount)}
                            maxButtons={5}
                            activePage={this.state.activePage}
                            onSelect={this.handlePagination}/>
                </div>

                <Navbar>
                    <Nav pullRight>
                        <NavItem>
                            <Button bsStyle="primary"
                                    disabled={isPdfLoading}
                                    onClick={!isPdfLoading ? this.handlePrintOderGrid : null}>
                                {isPdfLoading ? 'Generujem...' : 'Tlač zoznamu'}
                            </Button>
                        </NavItem>
                        <NavItem>
                            <Button bsStyle="primary"
                                    disabled={isXlsLoading}
                                    onClick={!isXlsLoading ? this.handleExportOderGrid : null}>
                                {isXlsLoading ? 'Generujem...' : 'Export do XLS'}
                            </Button>
                        </NavItem>
                        <NavItem>
                        </NavItem>
                    </Nav>
                </Navbar>

            </div>
        );
    }
}

export default withRouter(Dimensions()(OrderGrid));