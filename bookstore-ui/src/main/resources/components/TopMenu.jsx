import React from 'react';
import {
    Navbar,
    Nav,
    NavItem,
    NavDropdown,
    MenuItem
} from 'react-bootstrap';
import {withRouter, NavLink, Link} from 'react-router-dom';
import getAPIPath from '../utils/FetchClient.js';

class TopMenu extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            loggedUser: {login: ""}
        };
        this.handleLogout = this.handleLogout.bind(this)
    }

    componentDidMount() {
        fetch(getAPIPath() + "/loggedUser", {
            method: "GET",
            headers: {
                "Accept": "application/json"
            },
            credentials: 'include'
        })
            .then((response) => {
                if (response.status == 401) {
                    this.props.history.push('/login')
                }
                return response.json();
            })
            .then((data) => {
                this.setState({loggedUser: data})
            })
            .catch(function(error) {
                console.log('error', error)
                throw error
            })
    }

    handleLogout(event) {
        fetch(getAPIPath() + "/logout", {
            method: "GET",
            headers: {
                "Accept": "application/json"
            },
            credentials: 'include'
        })
            .then((response) => {
                this.props.history.push('/login')
                window.location.reload()
            })
            .catch(function (error) {
                console.log('error', error)
                window.location.reload()
            })
    }

    getIsActive = () => {
        return true;
    }

    render() {
        const { location } = this.props;
        const orderActive = location.pathname === "/" ? true : location.pathname.match(/^\/order/) ? true : false;
        const deliveryActive = location.pathname.match(/^\/delivery/) ? true : false;
        return (
            <div>
                <Navbar fixedTop={true}>
                    <Navbar.Header>
                        <Navbar.Brand>
                            DISTRIBUČNÝ SKLAD
                        </Navbar.Brand>
                    </Navbar.Header>
                    <Nav pullLeft>
                        <NavItem active={orderActive} href="#/">Objednávky</NavItem>
                        <NavItem active={deliveryActive} href="#/delivery">Dodacie listy</NavItem>
                    </Nav>
                    <Nav pullRight>
                        <NavDropdown title={this.state.loggedUser.login} id="basic-nav-dropdown">
                            <MenuItem onClick={this.handleLogout}>Odhlásiť</MenuItem>
                        </NavDropdown>
                    </Nav>
                </Navbar>
            </div>
        );
    }

}

export default withRouter(TopMenu);