package com.firstclazz.bookstore.infrastructure.models;

import sk.gov.edu.egovmessage.EGovMessage;

import java.util.UUID;

public class GovMessageInfo extends EGovMessage.Header.MessageInfo {

  public static final String BUSINESS_ID = "ee4ab1c1-6918-4b0a-9ece-9580c33d7359";

  public GovMessageInfo(MessageInfoBuilder builder) {
    this.clazz = builder.clazz;
    this.messageID = builder.messageID;
    this.correlationID = builder.correlationID;
    this.businessID = builder.businessID;
    this.channelInfoReply = builder.channelInfoReply;
  }

  public static class MessageInfoBuilder {

    private String clazz;

    private String messageID;

    private String correlationID;

    private String businessID;

    private String channelInfoReply;

    public MessageInfoBuilder(ActionEnum action) {
      this.messageID = UUID.randomUUID().toString();
      this.correlationID = UUID.randomUUID().toString();
      this.businessID = BUSINESS_ID;
      this.clazz = action.getAction();
    }

    public MessageInfoBuilder channelInfoReply(String channelInfoReply) {
      this.channelInfoReply = channelInfoReply;
      return this;
    }

    public MessageInfoBuilder next() {
      this.messageID = UUID.randomUUID().toString();
      return this;
    }

    public EGovMessage.Header.MessageInfo build() {
      return new GovMessageInfo( this );
    }

  }

}
