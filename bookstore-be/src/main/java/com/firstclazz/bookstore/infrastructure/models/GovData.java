package com.firstclazz.bookstore.infrastructure.models;

import sk.gov.edu.egovmessage.EGovMessage;

public class GovData extends EGovMessage.Body.Data {

  public GovData(DataBuilder builder) {
    this.any = builder.any;
  }

  public static class DataBuilder {

    private Object any;

    public DataBuilder any(Object any) {
      this.any = any;
      return this;
    }

    public EGovMessage.Body.Data build() {
      return new GovData( this );
    }

  }
}
