package com.firstclazz.bookstore.infrastructure.models;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "DeliveryNotes")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeliveryNotes")
public class GovDeliveryNotes extends GovAnyElement {

//    <ArrayOfDeliveryNote>
//        <DeliveryNote>
//            <ID>[ID*]</ID>
//            <Code>[CODE*]</Code>
//            <Delivered>[DELIVERED*]</Delivered>
//            <TookOverBy>[TOOKOVERBY]</TookOverBy>
//            <Info>[INFO]</Info>
//            <OrderCodes>
//            <OrderCode>[ORDERCODE*]</OrderCode>
//            </OrderCodes>
//            <ArrayOfItem>
//                <Item>
//                    <ID>[ITEMID*]</ID>
//                    <ItemOrder>[ITEMORDER]</ItemOrder>
//                    <BookEAN>[BOOKEAN*]</BookEAN>
//                    <BookISBN>[BOOKISBN*]</BookISBN>
//                    <Amount>[AMOUNT*]</Amount>
//                </Item>
//            </ArrayOfItem>
//        </DeliveryNote>
//    </ArrayOfDeliveryNote>

    //    <ArrayOfDeliveryNote>
    @XmlElement(name = "ArrayOfDeliveryNote")
    private ArrayOfDeliveryNote arrayOfDeliveryNote;

    public static class ArrayOfDeliveryNote extends GovAnyElement {

        public ArrayOfDeliveryNote() {
        }

        public ArrayOfDeliveryNote(List<DeliveryNote> deliveryNotes) {
            this.deliveryNotes= deliveryNotes;
        }

        @XmlElement(name = "DeliveryNote")
        private List<DeliveryNote> deliveryNotes;

        @XmlAccessorType(XmlAccessType.FIELD)
        public static class DeliveryNote extends GovAnyElement {

            @XmlElement(name = "ID")
            private String id;

            @XmlElement(name = "Code")
            private String code;

            @XmlElement(name = "Delivered")
            private String delivered;

            @XmlElement(name = "TookOverBy")
            private String tookOverBy;

            @XmlElement(name = "Info")
            private String info;

            @XmlElement(name = "OrderCodes")
            private List<OrderCode> orderCodes;

            @XmlAccessorType(XmlAccessType.FIELD)
            public static class OrderCode extends GovAnyElement {

                @XmlElement(name = "OrderCode")
                private String orderCode;

                public OrderCode() {
                }

                public OrderCode(String orderCode) {
                    this.orderCode = orderCode;
                }

                public String getOrderCode() {
                    return orderCode;
                }

                public void setOrderCode(String orderCode) {
                    this.orderCode = orderCode;
                }
            }

            @XmlElement(name = "Items")
            public ArrayOfItem arrayOfItem;

            public static class ArrayOfItem extends GovAnyElement {

                public ArrayOfItem() {
                }

                public ArrayOfItem(List<Item> items) {
                    this.items = items;
                }

                @XmlElement(name = "Item")
                private List<Item> items;

                @XmlAccessorType(XmlAccessType.FIELD)
                public static class Item extends GovAnyElement {

                    @XmlElement(name = "ID")
                    private String id;

                    @XmlElement(name = "ItemOrder")
                    private Integer itemOrder;

                    @XmlElement(name = "BookEAN")
                    private String bookEAN;

                    @XmlElement(name = "BookISBN")
                    private String bookISBN;

                    @XmlElement(name = "amount")
                    private Integer amount;

                    public String getId() {
                        return id;
                    }

                    public void setId(String id) {
                        this.id = id;
                    }

                    public Integer getItemOrder() {
                        return itemOrder;
                    }

                    public void setItemOrder(Integer itemOrder) {
                        this.itemOrder = itemOrder;
                    }

                    public String getBookEAN() {
                        return bookEAN;
                    }

                    public void setBookEAN(String bookEAN) {
                        this.bookEAN = bookEAN;
                    }

                    public String getBookISBN() {
                        return bookISBN;
                    }

                    public void setBookISBN(String bookISBN) {
                        this.bookISBN = bookISBN;
                    }

                    public Integer getAmount() {
                        return amount;
                    }

                    public void setAmount(Integer amount) {
                        this.amount = amount;
                    }
                }

                public List<Item> getItems() {
                    return items;
                }

                public void setItems(List<Item> arrayOfItem) {
                    this.items = arrayOfItem;
                }
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getDelivered() {
                return delivered;
            }

            public void setDelivered(String delivered) {
                this.delivered = delivered;
            }

            public String getTookOverBy() {
                return tookOverBy;
            }

            public void setTookOverBy(String tookOverBy) {
                this.tookOverBy = tookOverBy;
            }

            public String getInfo() {
                return info;
            }

            public void setInfo(String info) {
                this.info = info;
            }

            public List<OrderCode> getOrderCodes() {
                return orderCodes;
            }

            public void setOrderCodes(List<OrderCode> orderCodes) {
                this.orderCodes = orderCodes;
            }

            public ArrayOfItem getArrayOfItem() {
                return arrayOfItem;
            }

            public void setArrayOfItem(ArrayOfItem arrayOfItem) {
                this.arrayOfItem = arrayOfItem;
            }
        }

        public List<DeliveryNote> getDeliveryNotes() {
            return deliveryNotes;
        }

        public void setDeliveryNotes(List<DeliveryNote> deliveryNotes) {
            this.deliveryNotes = deliveryNotes;
        }
    }

    public ArrayOfDeliveryNote getArrayOfDeliveryNote() {
        return arrayOfDeliveryNote;
    }

    public void setArrayOfDeliveryNote(ArrayOfDeliveryNote arrayOfDeliveryNote) {
        this.arrayOfDeliveryNote = arrayOfDeliveryNote;
    }
}
