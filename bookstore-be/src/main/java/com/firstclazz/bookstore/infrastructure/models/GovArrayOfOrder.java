package com.firstclazz.bookstore.infrastructure.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonValue;

import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "ArrayOfOrder")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfOrder")
public class GovArrayOfOrder extends GovAnyElement {

    @XmlElement(name = "EGovOrder")
    private Order order;

    public GovArrayOfOrder() {
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Order {

        @JsonProperty("ID")
        private String id;

        @JsonProperty("Code")
        private String code;

        @JsonProperty("SchoolID")
        private String schoolID;

        @JsonProperty("SchoolEDUID")
        private String schoolEDUID;

        @JsonProperty("SchoolCode")
        private String schoolCode;

        @JsonProperty("Person")
        private String person;

        @JsonProperty("Year")
        private String year;

        @JsonProperty("Addressee")
        private String addressee;

        @JsonProperty("Street")
        private String street;

        @JsonProperty("PostalCode")
        private String postalCode;

        @JsonProperty("Municipality")
        private String municipality;

        @JsonProperty("District")
        private String district;

        @JsonProperty("Tel")
        private String tel;

        @JsonProperty("Email")
        private String email;

        @JsonProperty("Created")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
        private Date created;

        @JsonProperty("Confirmed")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
        private Date confirmed;

        @JsonProperty("Revision")
        private String revision;

        @JsonProperty("State")
        private String state;

        @JsonProperty("Status")
        private String status;

        @XmlElement(name = "Items")
        @JsonProperty("Items")
        private List<OrderItem> orderItems;

        public String getId() {
            return id;
        }

        public String getCode() {
            return code;
        }

        public String getSchoolID() {
            return schoolID;
        }

        public String getSchoolEDUID() {
            return schoolEDUID;
        }

        public String getSchoolCode() {
            return schoolCode;
        }

        public String getPerson() {
            return person;
        }

        public String getYear() {
            return year;
        }

        public String getAddressee() {
            return addressee;
        }

        public String getStreet() {
            return street;
        }

        public String getPostalCode() {
            return postalCode;
        }

        public String getMunicipality() {
            return municipality;
        }

        public String getDistrict() {
            return district;
        }

        public String getTel() {
            return tel;
        }

        public String getEmail() {
            return email;
        }

        public Date getCreated() {
            return created;
        }

        public Date getConfirmed() {
            return confirmed;
        }

        public String getRevision() {
            return revision;
        }

        public String getState() {
            return state;
        }

        public String getStatus() {
            return status;
        }

        public List<OrderItem> getOrderItems() {
            return orderItems;
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        public static class OrderItem {

            @XmlElement(name = "ID")
            @JsonProperty("ID")
            private String id;

            @JsonProperty("BookID")
            private String bookID;

            @JsonProperty("BookEAN")
            private String bookEAN;

            @JsonProperty("BookISBN")
            private String bookISBN;

            @JsonProperty("Amount")
            private Integer amount;

            public String getId() {
                return id;
            }

            public String getBookID() {
                return bookID;
            }

            public String getBookEAN() {
                return bookEAN;
            }

            public String getBookISBN() {
                return bookISBN;
            }

            public Integer getAmount() {
                return amount;
            }

        }

    }

}
