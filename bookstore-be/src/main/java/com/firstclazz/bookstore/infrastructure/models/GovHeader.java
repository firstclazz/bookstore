package com.firstclazz.bookstore.infrastructure.models;

import sk.gov.edu.egovmessage.EGovMessage;

public class GovHeader extends EGovMessage.Header {

  public GovHeader(HeaderBuilder builder) {
    this.messageInfo = builder.messageInfo;
    this.senderInfo = builder.senderInfo;
  }

  public static class HeaderBuilder {

    private MessageInfo messageInfo;

    private SenderInfo senderInfo;

    public HeaderBuilder messageInfo(MessageInfo messageInfo) {
      this.messageInfo = messageInfo;
      return this;
    }

    public HeaderBuilder senderInfo(SenderInfo senderInfo) {
      this.senderInfo = senderInfo;
      return this;
    }

    public EGovMessage.Header build() {
      return new GovHeader( this );
    }

  }

}
