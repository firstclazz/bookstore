package com.firstclazz.bookstore.infrastructure.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Parameters")
@XmlAccessorType( XmlAccessType.FIELD)
public class GovDeliveryParams {

    @XmlElement(name = "DeliveryNotes")
    private GovDeliveryNotes deliveryNotes;

    public GovDeliveryParams() {
    }

    public GovDeliveryParams(GovDeliveryNotes deliveryNotes) {
        this.deliveryNotes = deliveryNotes;
    }

    public GovDeliveryNotes getDeliveryNotes() {
        return deliveryNotes;
    }

    public void setDeliveryNotes(GovDeliveryNotes deliveryNotes) {
        this.deliveryNotes = deliveryNotes;
    }
}
