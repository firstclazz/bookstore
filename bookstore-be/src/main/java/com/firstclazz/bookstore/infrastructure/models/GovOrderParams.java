package com.firstclazz.bookstore.infrastructure.models;

import javax.xml.bind.annotation.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@XmlRootElement(name = "Parameters")
@XmlAccessorType( XmlAccessType.FIELD)
public class GovOrderParams extends GovAnyElement {

  @XmlElement(name = "LastSync")
  private String lastSync;

  @XmlElement(name = "PageSize")
  private Integer pageSize;

  @XmlElement(name = "Page")
  private Integer page;

  @XmlElement(name = "Json")
  private Boolean json;

  @XmlElement(name = "ID")
  private String id;

  @XmlElement(name = "EanCode")
  private String eanCode;

  public GovOrderParams() {
  }

  public GovOrderParams(OrderParamsBuilder builder) {

    this.lastSync = builder.lastSync;
    this.pageSize = builder.pageSize;
    this.page = builder.page;
    this.json = builder.json;
    this.id = builder.id;
    this.eanCode = builder.eanCode;

  }

  public static class OrderParamsBuilder {

    private String lastSync;

    private Integer pageSize;

    private Integer page;

    private Boolean json;

    private String id;

    private String eanCode;

    public OrderParamsBuilder lastSync(Date lastSync) {
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
      this.lastSync = lastSync == null ? null : format.format(lastSync);
      return this;
    }

    public OrderParamsBuilder lastSync(String lastSync) {
      this.lastSync = lastSync;
      return this;
    }

    public OrderParamsBuilder pageSize(Integer pageSize) {
      this.pageSize = pageSize;
      return this;
    }

    public OrderParamsBuilder page(Integer page) {
      this.page = page;
      return this;
    }

    public OrderParamsBuilder json(Boolean json) {
      this.json = json;
      return this;
    }

    public OrderParamsBuilder id(String id) {
      this.id = id;
      return this;
    }

    public OrderParamsBuilder eanCode(String eanCode) {
      this.eanCode = eanCode;
      return this;
    }

    public Object build() {
      return new GovOrderParams( this );
    }

  }

}
