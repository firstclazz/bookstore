package com.firstclazz.bookstore.infrastructure.models;

import sk.gov.edu.egovmessage.EGovMessage;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GovMessage extends EGovMessage {

  public static final String ENVELOPE_VERSION = "2.0";

    public GovMessage() {
    }

    public GovMessage(MessageBuilder builder) {
    this.envelopeVersion = builder.envelopeVersion;
    this.header = builder.header;
    this.body = builder.body;
    this.any = builder.any;
  }

  public static class MessageBuilder {

    private String envelopeVersion;

    private EGovMessage.Header header;

    private EGovMessage.Body body;

    private Object any;

    public MessageBuilder() {
      this.envelopeVersion = ENVELOPE_VERSION;
    }

    public MessageBuilder header(Header header) {
      this.header = header;
      return this;
    }

    public MessageBuilder body(Body body) {
      this.body = body;
      return this;
    }

    public MessageBuilder any(Object any) {
      this.any = any;
      return this;
    }

    public EGovMessage build() {
      return new GovMessage( this );
    }

  }

}
