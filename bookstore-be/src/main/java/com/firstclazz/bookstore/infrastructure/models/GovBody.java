package com.firstclazz.bookstore.infrastructure.models;

import sk.gov.edu.egovmessage.EGovMessage;

public class GovBody extends EGovMessage.Body {

  public GovBody(BodyBuilder builder) {
    this.data = builder.data;
  }

  public static class BodyBuilder {

    private EGovMessage.Body.Data data;

    public BodyBuilder data(Data data) {
      this.data = data;
      return this;
    }

    public EGovMessage.Body build() {
      return new GovBody(this);
    }

  }

}
