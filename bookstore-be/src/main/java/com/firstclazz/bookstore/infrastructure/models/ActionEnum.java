package com.firstclazz.bookstore.infrastructure.models;

public enum ActionEnum {

  GET_ORDERS( "EP-GetOrders" ),
  GET_ORDERS_FOR_DISTRIBUTION( "EP-GetOrdersForDistribution" ),
  SET_DELIVERY_NOTICE("EP-SetDeliveryNotes");

  private String action;

  private ActionEnum(String action) {
    this.action = action;
  }

  public String getAction() {
    return action;
  }

}
