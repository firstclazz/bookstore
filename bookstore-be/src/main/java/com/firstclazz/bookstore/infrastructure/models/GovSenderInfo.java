package com.firstclazz.bookstore.infrastructure.models;

import sk.gov.edu.egovmessage.EGovMessage;

public class GovSenderInfo extends EGovMessage.Header.SenderInfo {

  public GovSenderInfo(SenderInfoBuilder builder ) {
    this.securityToken = builder.securityToken;
    this.userContext = builder.userContext;
  }

  public static class SenderInfoBuilder {

    private EGovMessage.Header.SenderInfo.SecurityToken securityToken;

    private EGovMessage.Header.SenderInfo.UserContext userContext;

    public SenderInfoBuilder securityToken(SecurityToken securityToken) {
      this.securityToken = securityToken;
      return this;
    }

    public SenderInfoBuilder userContext(UserContext userContext) {
      this.userContext = userContext;
      return this;
    }

    public EGovMessage.Header.SenderInfo build() {
      return new GovSenderInfo( this );
    }

  }

}
