package com.firstclazz.bookstore.infrastructure.factories;

import com.firstclazz.bookstore.infrastructure.models.*;
import com.firstclazz.bookstore.repositories.entities.Delivery;
import com.firstclazz.bookstore.repositories.entities.DeliveryItem;
import org.springframework.stereotype.Component;
import sk.gov.edu.egovmessage.EGovMessage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class GovMessageFactory {

    public EGovMessage createOrdersRequest(int page, int pageSize, Date lastSync) {
        return new GovMessage.MessageBuilder()
                .header(prepareMessageHeader(ActionEnum.GET_ORDERS))
                .body(preparMessageBodyGetOrders(page, pageSize, null, lastSync))
                .build();
    }

    private EGovMessage.Header prepareMessageHeader(ActionEnum action) {
        return new GovHeader.HeaderBuilder()
                .messageInfo(new GovMessageInfo.MessageInfoBuilder(action).build())
                .build();
    }

    private EGovMessage.Body preparMessageBodyGetOrders(int page, int pageSize, String id, Date lastSync) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        return new GovBody.BodyBuilder()
                .data(new GovData.DataBuilder()
                        .any(new GovOrderParams.OrderParamsBuilder()
                                .page(page)
                                .pageSize(pageSize)
                                .json(true)
                                .id(id)
                                .lastSync(lastSync == null ? null : format.format(lastSync))
                                .build())
                        .build())
                .build();
    }

    public EGovMessage setDeliveryNoticeRequest(List<Delivery> deliveries) {
        return new GovMessage.MessageBuilder()
                .header(prepareMessageHeader(ActionEnum.SET_DELIVERY_NOTICE))
                .body(prepareMessageBodySetDeliveryNotice(deliveries))
                .build();
    }

    public EGovMessage.Body prepareMessageBodySetDeliveryNotice(List<Delivery> deliveries) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");

        GovDeliveryNotes govDeliveryNotes = new GovDeliveryNotes();
        govDeliveryNotes.setArrayOfDeliveryNote(new GovDeliveryNotes.ArrayOfDeliveryNote(new ArrayList<>()));

        for (Delivery delivery : deliveries) {
            GovDeliveryNotes.ArrayOfDeliveryNote.DeliveryNote deliveryNote = new GovDeliveryNotes.ArrayOfDeliveryNote.DeliveryNote();
            deliveryNote.setId(delivery.getId());
            deliveryNote.setCode(delivery.getDeliveryNumber());
            deliveryNote.setDelivered(format.format(delivery.getConfirmed()).toString());
            deliveryNote.setTookOverBy(null);
            deliveryNote.setInfo(null);
            deliveryNote.setOrderCodes(new ArrayList<>());
            deliveryNote.getOrderCodes().add(
                    new GovDeliveryNotes.ArrayOfDeliveryNote.DeliveryNote.OrderCode(delivery.getOrder().getCode()));
            deliveryNote.setArrayOfItem(new GovDeliveryNotes.ArrayOfDeliveryNote.DeliveryNote.ArrayOfItem(new ArrayList<>()));
            if (delivery.getDeliveryItems() != null && delivery.getDeliveryItems().size() > 0) {
                for (DeliveryItem deliveryItem : delivery.getDeliveryItems()) {
                    GovDeliveryNotes.ArrayOfDeliveryNote.DeliveryNote.ArrayOfItem.Item item = new GovDeliveryNotes.ArrayOfDeliveryNote.DeliveryNote.ArrayOfItem.Item();
                    item.setId(deliveryItem.getId());
                    item.setItemOrder(null);
                    item.setBookEAN(deliveryItem.getBookEan());
                    item.setBookISBN(deliveryItem.getBookIsbn());
                    item.setAmount(deliveryItem.getAmount());
                    deliveryNote.getArrayOfItem().getItems().add(item);
                }
            }
            govDeliveryNotes.getArrayOfDeliveryNote().getDeliveryNotes().add(deliveryNote);
        }

        GovDeliveryParams deliveryParams = new GovDeliveryParams(govDeliveryNotes);

        return new GovBody.BodyBuilder()
                .data(new GovData.DataBuilder()
                        .any(deliveryParams)
                        .build())
                .build();
    }

}
