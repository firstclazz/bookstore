package com.firstclazz.bookstore.infrastructure.models;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }

    public GovOrderParams createGovOrderParams() {
        return new GovOrderParams();
    }

    public GovArrayOfOrder createGovArrayOfOrder() {
        return new GovArrayOfOrder();
    }

    public GovArrayOfOrder.Order createGovArrayOfOrderOrder() {
        return new GovArrayOfOrder.Order();
    }

//  public GovArrayOfOrder.EGovOrder.Items createGovArrayOfOrderOrderItems() {
//    return new GovArrayOfOrder.EGovOrder.Items();
//  }
//
//  public GovArrayOfOrder.EGovOrder.Items.OrderItem createGovArrayOfOrderOrderItemsOrderItem() {
//    return new GovArrayOfOrder.EGovOrder.Items.OrderItem();
//  }

    public GovArrayOfOrder.Order.OrderItem createGovArrayOfOrderOrderOrderItem() {
        return new GovArrayOfOrder.Order.OrderItem();
    }

}
