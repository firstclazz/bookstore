package com.firstclazz.bookstore.infrastructure.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({
        GovOrderParams.class,
        GovArrayOfOrder.class,
        GovDeliveryParams.class,
        GovDeliveryNotes.class
})
public class GovAnyElement {
}
