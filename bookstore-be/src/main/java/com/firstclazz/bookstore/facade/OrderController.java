package com.firstclazz.bookstore.facade;

import com.firstclazz.bookstore.repositories.entities.EGovOrder;
import com.firstclazz.bookstore.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@RestController
@CrossOrigin
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping(value = "/orders", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<EGovOrder> getOrders(@RequestParam(defaultValue = "1") int page,
                                     @RequestParam(defaultValue = "10") int pageSize,
                                     @RequestParam(required = false) String sortColumn,
                                     @RequestParam(required = false) String sortDir,
                                     @RequestParam(required = false) String status,
                                     @RequestParam(required = false) String code,
                                     @RequestParam(required = false) String address,
                                     @RequestParam(required = false) String street,
                                     @RequestParam(required = false) String municipality) {
        return orderService.getOrders(page, pageSize, sortColumn, sortDir, status, code, address, street, municipality);
    }

    @GetMapping(value = "/order/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public EGovOrder getOrderDetail(@PathVariable String id) {
        return orderService.getOrderDetail(id);
    }

    @GetMapping(value = "/ordersCount", produces = MediaType.APPLICATION_JSON_VALUE)
    public Long getOrdersCount(@RequestParam(required = false) String status,
                               @RequestParam(required = false) String code,
                               @RequestParam(required = false) String address,
                               @RequestParam(required = false) String street,
                               @RequestParam(required = false) String municipality) {
        return orderService.getOrdersCount(status, code, address, street, municipality);
    }

    @GetMapping("/ordersPdf")
    public ResponseEntity<byte[]> getOrdersPdf(@RequestParam(required = false) String sortColumn,
                                               @RequestParam(required = false) String sortDir,
                                               @RequestParam(required = false) String status,
                                               @RequestParam(required = false) String code,
                                               @RequestParam(required = false) String address,
                                               @RequestParam(required = false) String street,
                                               @RequestParam(required = false) String municipality) throws IOException {

        String pdfFilePath = orderService.getOrdersPdf(sortColumn, sortDir, status, code, address, street, municipality);
        byte[] contents = Files.readAllBytes(Paths.get(pdfFilePath));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        String filename = "orders_export.pdf";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity(contents, headers, HttpStatus.OK);
        return response;
    }

    @GetMapping("/ordersXls")
    public ResponseEntity<byte[]> getOrdersXls(@RequestParam(required = false) String sortColumn,
                                               @RequestParam(required = false) String sortDir,
                                               @RequestParam(required = false) String status,
                                               @RequestParam(required = false) String code,
                                               @RequestParam(required = false) String address,
                                               @RequestParam(required = false) String street,
                                               @RequestParam(required = false) String municipality) throws IOException {

        String pdfFilePath = orderService.getOrdersXls(sortColumn, sortDir, status, code, address, street, municipality);
        byte[] contents = Files.readAllBytes(Paths.get(pdfFilePath));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
        String filename = "orders_export.xlsx";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity(contents, headers, HttpStatus.OK);
        return response;
    }

}
