package com.firstclazz.bookstore.facade;

import com.firstclazz.bookstore.facade.models.UserModel;
import com.firstclazz.bookstore.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public UserModel getLoggedUser() {
        return userService.getLoggedUser();
    }

}
