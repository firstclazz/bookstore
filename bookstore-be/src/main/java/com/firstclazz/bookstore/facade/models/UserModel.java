package com.firstclazz.bookstore.facade.models;

public class UserModel {

    private String login;

    public UserModel(UserModelBuilder userModelBuilder) {
        this.login = userModelBuilder.login;
    }

    public String getLogin() {
        return login;
    }

    public static class UserModelBuilder {

        private String login;

        public UserModelBuilder() {
        }

        public UserModelBuilder login(String login) {
            this.login = login;
            return this;
        }

        public UserModel build() {
            return new UserModel(this);
        }
    }

}
