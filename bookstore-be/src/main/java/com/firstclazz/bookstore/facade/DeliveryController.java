package com.firstclazz.bookstore.facade;

import com.firstclazz.bookstore.repositories.entities.Delivery;
import com.firstclazz.bookstore.services.DeliveryService;
import com.firstclazz.bookstore.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@RestController
@CrossOrigin
public class DeliveryController {

    @Autowired
    private DeliveryService deliveryService;

    @GetMapping(value = "/deliveries", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Delivery> getDeliveries(@RequestParam(defaultValue = "1") int page,
                                        @RequestParam(defaultValue = "10") int pageSize) {
        return deliveryService.getDeliveries(page, pageSize);
    }

    @GetMapping(value = "/deliveryCount", produces = MediaType.APPLICATION_JSON_VALUE)
    public Long getOrdersCount() {
        return this.deliveryService.getDeliveryCount();
    }

    @GetMapping(value = "/delivery/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Delivery getDeliveryDetail(@PathVariable(required = true) String id) {
        return deliveryService.getDeliveryDetail(id);
    }

    @PutMapping("/delivery/{orderId}")
    public String createDelivery(@PathVariable String orderId) {
        return deliveryService.createDelivery(orderId);
    }

    @PostMapping(value = "/delivery/{deliveryId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void saveDelivery(@PathVariable String deliveryId,
                             @RequestBody Delivery delivery) {
        deliveryService.updateDelivery(deliveryId, delivery);
    }

    @PostMapping(value = "/confirmDelivery/{deliveryId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void confirmDelivery(@PathVariable String deliveryId,
                             @RequestBody Delivery delivery) {
        deliveryService.confirmDelivery(deliveryId, delivery);
    }

    @GetMapping("/deliveryNotePdf/{id}")
    public ResponseEntity<byte[]> getDeliveryNotePdf(@PathVariable String id) throws IOException {
        String pdfFilePath = deliveryService.getDeliveryNotePdf(id);
        byte[] contents = Files.readAllBytes(Paths.get(pdfFilePath));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        String filename = "DodaciList.pdf";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity(contents, headers, HttpStatus.OK);
        return response;
    }

    @GetMapping("/deliveryNotesPdf")
    public ResponseEntity<byte[]> getDeliveryNotesPdf() throws IOException {
        String pdfFilePath = deliveryService.getDeliveryNotesPdf();
        byte[] contents = Files.readAllBytes(Paths.get(pdfFilePath));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        String filename = "DeliveryNotes.pdf";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity(contents, headers, HttpStatus.OK);
        return response;
    }

    @GetMapping("/deliveryNotesXls")
    public ResponseEntity<byte[]> getDeliveryNotesXls() throws IOException {
        String pdfFilePath = deliveryService.getDeliveryNotesXls();
        byte[] contents = Files.readAllBytes(Paths.get(pdfFilePath));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
        String filename = "DeliveryNotes.xlsx";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity(contents, headers, HttpStatus.OK);
        return response;
    }

}
