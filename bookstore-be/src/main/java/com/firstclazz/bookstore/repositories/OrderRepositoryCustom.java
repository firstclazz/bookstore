package com.firstclazz.bookstore.repositories;

import com.firstclazz.bookstore.repositories.entities.EGovOrder;

import java.util.List;

public interface OrderRepositoryCustom {

    void merge(EGovOrder order);

    List<EGovOrder> findOrders(Integer page, Integer pageSize, String sortColumn,
                               String sortDir, String status, String code,
                               String address, String street, String municipality);

    Long getOrdersCount(String status, String code, String address, String street, String municipality);

}
