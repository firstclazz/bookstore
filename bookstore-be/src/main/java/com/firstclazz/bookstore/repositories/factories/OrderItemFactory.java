package com.firstclazz.bookstore.repositories.factories;

import com.firstclazz.bookstore.infrastructure.models.GovArrayOfOrder;
import com.firstclazz.bookstore.repositories.entities.OrderItem;

import java.util.ArrayList;
import java.util.List;

public class OrderItemFactory {

    public OrderItem createOrderItemFromGovOrderItem(GovArrayOfOrder.Order.OrderItem govOrderItem) {
        OrderItem.OrderItemBuilder builder = new OrderItem.OrderItemBuilder()
                .id(govOrderItem.getId())
                .bookId(govOrderItem.getBookID())
                .bookEan(govOrderItem.getBookEAN())
                .bookIsbn(govOrderItem.getBookISBN())
                .amount(govOrderItem.getAmount());

        return builder.build();
    }

    public List<OrderItem> createOrderItemsFromGovOrderItems(List<GovArrayOfOrder.Order.OrderItem> govOrderItems) {
        List<OrderItem> orderItems = new ArrayList<>();
        if (govOrderItems != null) {
            for (GovArrayOfOrder.Order.OrderItem govOrder : govOrderItems) {
                orderItems.add(createOrderItemFromGovOrderItem(govOrder));
            }
        }
        return orderItems;
    }

}
