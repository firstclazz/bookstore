package com.firstclazz.bookstore.repositories.entities;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "DELIVERY")
public class Delivery {

    @Id
    @Column(name = "ID")
    @Size(max = 36)
    private String id;

    @Column(name = "DELIVERY_NUMBER")
    private String deliveryNumber;

    @Column(name = "CREATED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CONFIRMED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date confirmed;

    @Column(name = "CONFIRMED_BY")
    private String confirmedBy;

    @Column(name = "SYNCED")
    private Boolean synced;

    @ManyToOne
    @JoinColumn(name = "ORDER_ID")
    private EGovOrder order;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "DELIVERY_ID", referencedColumnName = "ID")
    private List<DeliveryItem> deliveryItems;

    public Delivery() {
    }

    public Delivery(Delivery.DeliveryBuilder builder) {
        this.id = builder.id;
        this.deliveryNumber = builder.deliveryNumber;
        this.created = builder.created;
        this.createdBy = builder.createdBy;
        this.confirmed = builder.confirmed;
        this.confirmedBy = builder.confirmedBy;
        this.synced = builder.synced;
        this.order = builder.order;
        this.deliveryItems = builder.deliveryItems;
    }

    public String getId() {
        return id;
    }

    public String getDeliveryNumber() {
        return deliveryNumber;
    }

    public Date getCreated() {
        return created;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getConfirmed() {
        return confirmed;
    }

    public String getConfirmedBy() {
        return confirmedBy;
    }

    public Boolean getSynced() {
        return synced;
    }

    public void setSynced(Boolean synced) {
        this.synced = synced;
    }

    public EGovOrder getOrder() {
        return order;
    }

    public void setConfirmed(Date confirmed) {
        this.confirmed = confirmed;
    }

    public void setConfirmedBy(String confirmedBy) {
        this.confirmedBy = confirmedBy;
    }

    public List<DeliveryItem> getDeliveryItems() {
        return deliveryItems;
    }

    public void setDeliveryItems(List<DeliveryItem> deliveryItems) {
        this.deliveryItems = deliveryItems;
    }

    @Transient
    public Integer getDeliveryItemsCount() {
        return this.deliveryItems != null ? this.deliveryItems.size() : 0;
    }

    public static class DeliveryBuilder {
        private String id;
        private String deliveryNumber;
        private Date created;
        private String createdBy;
        private Date confirmed;
        private String confirmedBy;
        private Boolean synced;
        private EGovOrder order;
        private List<DeliveryItem> deliveryItems;

        public DeliveryBuilder() {
        }

        public Delivery.DeliveryBuilder id(String id) {
            this.id = id;
            return this;
        }

        public Delivery.DeliveryBuilder deliveryNumber(String deliveryNumber) {
            this.deliveryNumber = deliveryNumber;
            return this;
        }

        public Delivery.DeliveryBuilder created(Date created) {
            this.created = created;
            return this;
        }

        public Delivery.DeliveryBuilder createdBy(String createdBy) {
            this.createdBy = createdBy;
            return this;
        }

        public Delivery.DeliveryBuilder confirmed(Date confirmed) {
            this.confirmed = confirmed;
            return this;
        }

        public Delivery.DeliveryBuilder confirmedBy(String confirmedBy) {
            this.confirmedBy = confirmedBy;
            return this;
        }

        public Delivery.DeliveryBuilder synced(Boolean synced) {
            this.synced = synced;
            return this;
        }

        public Delivery.DeliveryBuilder order(EGovOrder order) {
            this.order = order;
            return this;
        }

        public Delivery.DeliveryBuilder deliveryItems(List<DeliveryItem> deliveryItems) {
            this.deliveryItems = deliveryItems;
            return this;
        }

        public Delivery build() {
            return new Delivery(this);
        }
    }

}
