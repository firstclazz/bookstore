package com.firstclazz.bookstore.repositories.factories;

import com.firstclazz.bookstore.infrastructure.models.GovArrayOfOrder;
import com.firstclazz.bookstore.repositories.entities.EGovOrder;

public class OrderFactory {

    public EGovOrder createOrderFromGovOrder(GovArrayOfOrder.Order govOrder) {
        EGovOrder.OrderBuilder builder = new EGovOrder.OrderBuilder()
                .id(govOrder.getId())
                .code(govOrder.getCode())
                .schoolID(govOrder.getSchoolID())
                .schoolEDUID(govOrder.getSchoolEDUID())
                .schoolCode(govOrder.getSchoolCode())
                .person(govOrder.getPerson())
                .year(govOrder.getYear())
                .address(govOrder.getAddressee())
                .street(govOrder.getStreet())
                .postalCode(govOrder.getPostalCode())
                .municipality(govOrder.getMunicipality())
                .district(govOrder.getDistrict())
                .phone(govOrder.getTel())
                .email(govOrder.getEmail())
                .created(govOrder.getCreated())
                .confirmed(govOrder.getConfirmed())
                .revision(govOrder.getRevision())
                .state(govOrder.getState())
                .status(govOrder.getStatus())
                .orderItems(new OrderItemFactory().createOrderItemsFromGovOrderItems(govOrder.getOrderItems()));
        return builder.build();
    }
}
