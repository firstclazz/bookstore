package com.firstclazz.bookstore.repositories;

import com.firstclazz.bookstore.repositories.entities.Delivery;

import java.util.List;

public interface DeliveryRepositoryCustom {

    List<Delivery> findDeliveries(Integer page, Integer pageSize);

    Long getDeliveryCount();

}
