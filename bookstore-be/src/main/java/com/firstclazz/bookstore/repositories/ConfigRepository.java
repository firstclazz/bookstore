package com.firstclazz.bookstore.repositories;

import com.firstclazz.bookstore.repositories.entities.Config;
import org.springframework.data.repository.CrudRepository;

public interface ConfigRepository extends CrudRepository<Config, String> {
}
