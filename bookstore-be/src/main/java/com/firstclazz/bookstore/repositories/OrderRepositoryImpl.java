package com.firstclazz.bookstore.repositories;

import com.firstclazz.bookstore.repositories.entities.EGovOrder;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

public class OrderRepositoryImpl implements OrderRepositoryCustom {

    @Autowired
    private OrderRepository orderRepository;

    @PersistenceContext
    private EntityManager entityManager;

    public static Logger LOG = LoggerFactory.getLogger(OrderRepositoryImpl.class);

    public void merge(EGovOrder order) {
        EGovOrder oldOrder = this.orderRepository.findOne(order.getId());
        if (oldOrder != null) {
            order.setDeliveryCreated(oldOrder.getDeliveryCreated());
        }

        this.orderRepository.save(order);
    }

    @Override
    public List<EGovOrder> findOrders(Integer page, Integer pageSize, String sortColumn, String sortDir,
                                      String status, String code, String address, String street, String municipality) {
        LOG.debug("Fetch order page: " + page + " pageSize:" + pageSize);

        Integer offset = null;
        if (page != null && pageSize != null) {
            offset = (page - 1) * pageSize - 1;
            offset = offset < 0 ? 0 : offset;
        }

        String jpql = "SELECT o FROM EGovOrder o";
        jpql = jpql + " WHERE 1=1";

        jpql += StringUtils.isBlank(status) ? "" : " AND status='" + status + "'";
        jpql += StringUtils.isBlank(code) ? "" : " AND UPPER(code) like'%" + code.toUpperCase() + "%'";
        jpql += StringUtils.isBlank(address) ? "" : " AND UPPER(address) like'%" + address.toUpperCase() + "%'";
        jpql += StringUtils.isBlank(street) ? "" : " AND UPPER(street) like'%" + street.toUpperCase() + "%'";
        jpql += StringUtils.isBlank(municipality) ? "" : " AND UPPER(municipality) like'%" + municipality.toUpperCase() + "%'";

        jpql += StringUtils.isBlank(sortColumn) ? "" : " ORDER BY " + sortColumn + " " + sortDir;

        Query query = entityManager.createQuery(jpql);
        if (offset != null) {
            query.setFirstResult(offset);
        }
        if (pageSize != null) {
            query.setMaxResults(pageSize);
        }
        return query.getResultList();
    }

    @Override
    public Long getOrdersCount(String status, String code, String address, String street, String municipality) {
        String jpql = "SELECT COUNT(o) FROM EGovOrder o";

        jpql += " WHERE 1=1";
        jpql += StringUtils.isBlank(status) ? "" : " AND status='" + status + "'";
        jpql += StringUtils.isBlank(code) ? "" : " AND UPPER(code) like'%" + code.toUpperCase() + "%'";
        jpql += StringUtils.isBlank(address) ? "" : " AND UPPER(address) like'%" + address.toUpperCase() + "%'";
        jpql += StringUtils.isBlank(street) ? "" : " AND UPPER(street) like'%" + street.toUpperCase() + "%'";
        jpql += StringUtils.isBlank(municipality) ? "" : " AND UPPER(municipality) like'%" + municipality.toUpperCase() + "%'";

        return (Long) entityManager.createQuery(jpql).getSingleResult();
    }

}
