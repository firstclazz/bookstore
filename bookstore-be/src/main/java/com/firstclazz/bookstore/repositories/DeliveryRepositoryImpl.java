package com.firstclazz.bookstore.repositories;

import com.firstclazz.bookstore.repositories.entities.Delivery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

public class DeliveryRepositoryImpl implements DeliveryRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;
    public static Logger LOG = LoggerFactory.getLogger(DeliveryRepositoryImpl.class);

    @Override
    public List<Delivery> findDeliveries(Integer page, Integer pageSize) {
        LOG.debug("Fetch delivery page: " + page + " pageSize:" + pageSize);

        Integer offset = null;
        if (page != null && pageSize != null) {
            offset = (page - 1) * pageSize - 1;
            offset = offset < 0 ? 0 : offset;
        }

        String jpql = "SELECT d FROM Delivery d";

        Query query = entityManager.createQuery(jpql);
        if (offset != null) {
            query.setFirstResult(offset);
        }
        if (pageSize != null) {
            query.setMaxResults(pageSize);
        }

        return query.getResultList();
    }

    @Override
    public Long getDeliveryCount() {
        String jpql = "SELECT COUNT(d) FROM Delivery d";
        return (Long) entityManager.createQuery(jpql).getSingleResult();
    }

}
