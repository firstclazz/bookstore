package com.firstclazz.bookstore.repositories.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "EGOV_ORDER")
public class EGovOrder {

    @Id
    @Column(name = "ID", nullable = false)
    @Size(max = 36)
    private String id;

    @Column(name = "ORDER_CODE")
    private String code;

    @Column(name = "SCHOOL_ID")
    private String schoolID;

    @Column(name = "SCHOOL_EDUID")
    private String schoolEDUID;

    @Column(name = "SCHOOL_CODE")
    private String schoolCode;

    @Column(name = "PERSON")
    private String person;

    @Column(name = "YEAR")
    private String year;

    @Column(name = "ADDRESS")
    @Size(max = 1000)
    private String address;

    @Column(name = "STREET")
    private String street;

    @Column(name = "POSTAL_CODE")
    private String postalCode;

    @Column(name = "MUNICIPALITY")
    private String municipality;

    @Column(name = "DISTRICT")
    private String district;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "CREATED")
    @Temporal(TemporalType.DATE)
    private Date created;

    @Column(name = "CONFIRMED")
    @Temporal(TemporalType.DATE)
    private Date confirmed;

    @Column(name = "REVISION")
    private String revision;

    @Column(name = "STATE")
    private String state;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "DELIVERY_CREATED")
    private Boolean deliveryCreated;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "ORDER_ID", referencedColumnName = "ID")
    private List<OrderItem> orderItems;

    public EGovOrder() {
    }

    public EGovOrder(EGovOrder.OrderBuilder builder) {
        this.id = builder.id;
        this.code = builder.code;
        this.schoolID = builder.schoolID;
        this.schoolEDUID = builder.schoolEDUID;
        this.schoolCode = builder.schoolCode;
        this.person = builder.person;
        this.year = builder.year;
        this.address = builder.address;
        this.street = builder.street;
        this.postalCode = builder.postalCode;
        this.municipality = builder.municipality;
        this.district = builder.district;
        this.phone = builder.phone;
        this.email = builder.email;
        this.created = builder.created;
        this.confirmed = builder.confirmed;
        this.revision = builder.revision;
        this.state = builder.state;
        this.status = builder.status;
        this.orderItems = builder.orderItems;
        this.deliveryCreated = builder.deliveryCreated;
    }

    public String getId() {
        return this.id;
    }

    public String getCode() {
        return this.code;
    }

    public String getSchoolID() {
        return this.schoolID;
    }

    public String getSchoolEDUID() {
        return this.schoolEDUID;
    }

    public String getSchoolCode() {
        return this.schoolCode;
    }

    public String getPerson() {
        return this.person;
    }

    public String getYear() {
        return this.year;
    }

    public String getAddress() {
        return this.address;
    }

    public String getStreet() {
        return this.street;
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    public String getMunicipality() {
        return this.municipality;
    }

    public String getDistrict() {
        return this.district;
    }

    public String getPhone() {
        return this.phone;
    }

    public String getEmail() {
        return this.email;
    }

    public Date getCreated() {
        return this.created;
    }

    public Date getConfirmed() {
        return this.confirmed;
    }

    public String getRevision() {
        return this.revision;
    }

    public String getState() {
        return this.state;
    }

    public String getStatus() {
        return this.status;
    }

    public Boolean getDeliveryCreated() {
        return this.deliveryCreated;
    }

    public void setDeliveryCreated(Boolean deliveryCreated) {
        this.deliveryCreated = deliveryCreated;
    }

    public List<OrderItem> getOrderItems() {
        return this.orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    @Transient
    public Integer getOrderItemsCount() {
        return this.orderItems.size();
    }

    @Transient
    public String getStatusName() {
        switch(status) {
            case "2":
                return "Zrušená";
            case "717660003":
                return "Na distribúciu";
            case "717660004":
                return "Č̌iastočne dodaná";
            case "717660007":
                return "Úplne dodaná";
            case "717660008":
                return "Presunutá";
            default:
                return "Neznámy";
        }

    }

    public static class OrderBuilder {
        private String id;
        private String code;
        private String schoolID;
        private String schoolEDUID;
        private String schoolCode;
        private String person;
        private String year;
        private String address;
        private String street;
        private String postalCode;
        private String municipality;
        private String district;
        private String phone;
        private String email;
        private Date created;
        private Date confirmed;
        private String revision;
        private String state;
        private String status;
        private Boolean deliveryCreated;
        private List<OrderItem> orderItems;

        public OrderBuilder() {
        }

        public EGovOrder.OrderBuilder id(String id) {
            this.id = id;
            return this;
        }

        public EGovOrder.OrderBuilder code(String code) {
            this.code = code;
            return this;
        }

        public EGovOrder.OrderBuilder schoolID(String schoolID) {
            this.schoolID = schoolID;
            return this;
        }

        public EGovOrder.OrderBuilder schoolEDUID(String schoolEDUID) {
            this.schoolEDUID = schoolEDUID;
            return this;
        }

        public EGovOrder.OrderBuilder schoolCode(String schoolCode) {
            this.schoolCode = schoolCode;
            return this;
        }

        public EGovOrder.OrderBuilder person(String person) {
            this.person = person;
            return this;
        }

        public EGovOrder.OrderBuilder year(String year) {
            this.year = year;
            return this;
        }

        public EGovOrder.OrderBuilder address(String address) {
            this.address = address;
            return this;
        }

        public EGovOrder.OrderBuilder street(String street) {
            this.street = street;
            return this;
        }

        public EGovOrder.OrderBuilder postalCode(String postalCode) {
            this.postalCode = postalCode;
            return this;
        }

        public EGovOrder.OrderBuilder municipality(String municipality) {
            this.municipality = municipality;
            return this;
        }

        public EGovOrder.OrderBuilder district(String district) {
            this.district = district;
            return this;
        }

        public EGovOrder.OrderBuilder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public EGovOrder.OrderBuilder email(String email) {
            this.email = email;
            return this;
        }

        public EGovOrder.OrderBuilder created(Date created) {
            this.created = created;
            return this;
        }

        public EGovOrder.OrderBuilder confirmed(Date confirmed) {
            this.confirmed = confirmed;
            return this;
        }

        public EGovOrder.OrderBuilder revision(String revision) {
            this.revision = revision;
            return this;
        }

        public EGovOrder.OrderBuilder state(String state) {
            this.state = state;
            return this;
        }

        public EGovOrder.OrderBuilder status(String status) {
            this.status = status;
            return this;
        }

        public EGovOrder.OrderBuilder deliveryCreated(Boolean deliveryCreated) {
            this.deliveryCreated = deliveryCreated;
            return this;
        }

        public EGovOrder.OrderBuilder orderItems(List<OrderItem> orderItems) {
            this.orderItems = orderItems;
            return this;
        }

        public EGovOrder build() {
            return new EGovOrder(this);
        }
    }
}
