package com.firstclazz.bookstore.repositories.entities;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "CONFIG")
public class Config {

    @Id
    @Column(name = "CONFIG_KEY")
    @Size(max = 36)
    private String configKey;

    @Column(name = "STRING_VALUE")
    private String stringValue;

    @Column(name = "INT_VALUE")
    private Integer intValue;

    @Column(name = "LONG_VALUE")
    private Long longValue;

    @Column(name = "FLOAT_VALUE")
    private Float floatValue;

    @Column(name = "DOUBLE_VALUE")
    private Double doubleValue;

    @Column(name = "DATE_VALUE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateValue;

    public Config() {
    }

    public Config(ConfigBuilder builder) {
        this.configKey = builder.configKey;
        this.stringValue = builder.stringValue;
        this.intValue = builder.intValue;
        this.longValue = builder.longValue;
        this.floatValue = builder.floatValue;
        this.doubleValue = builder.doubleValue;
        this.dateValue = builder.dateValue;
    }

    public String getConfigKey() {
        return configKey;
    }

    public String getStringValue() {
        return stringValue;
    }

    public Integer getIntValue() {
        return intValue;
    }

    public Long getLongValue() {
        return longValue;
    }

    public Float getFloatValue() {
        return floatValue;
    }

    public Double getDoubleValue() {
        return doubleValue;
    }

    public Date getDateValue() {
        return dateValue;
    }

    public static class ConfigBuilder {

        private String configKey;

        private String stringValue;

        private Integer intValue;

        private Long longValue;

        private Float floatValue;

        private Double doubleValue;

        private Date dateValue;

        public ConfigBuilder(ConfigEnum configKey) {
            this.configKey = configKey.name();
        }

        public ConfigBuilder stringValue(String stringValue) {
            this.stringValue = stringValue;
            return this;
        }

        public ConfigBuilder integerValue(Integer intValue) {
            this.intValue = intValue;
            return this;
        }

        public ConfigBuilder longValue(Long longValue) {
            this.longValue = longValue;
            return this;
        }

        public ConfigBuilder floatValue(Float floatValue) {
            this.floatValue = floatValue;
            return this;
        }

        public ConfigBuilder doubleValue(Double doubleValue) {
            this.doubleValue = doubleValue;
            return this;
        }

        public ConfigBuilder dateValue(Date dateValue) {
            this.dateValue = dateValue;
            return this;
        }

        public Config build() {
            return new Config(this);
        }

    }

}
