package com.firstclazz.bookstore.repositories;

import com.firstclazz.bookstore.repositories.entities.Delivery;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeliveryRepository extends CrudRepository<Delivery, String>, DeliveryRepositoryCustom {

    List<Delivery> findBySyncedIsFalseAndConfirmedIsNotNull();
}
