package com.firstclazz.bookstore.repositories;

import java.util.HashMap;
import java.util.Map;

public class BookRepository {

    private static BookRepository instance = null;

    Map<String, BookMetadata> bookMetadataMap;

    public static BookRepository getInstance() {
        if (instance == null) {
            instance = new BookRepository();
        }
        return instance;
    }

    protected BookRepository() {
        bookMetadataMap = new HashMap<>();
        bookMetadataMap.put("KOD_2", new BookMetadata.BookMetadataBuilder()
                .isbn("978-80-8146-049-4d")
                .name("Hupsov šlabikár Lipka 1. časť")
                .build());
        bookMetadataMap.put("KOD_3", new BookMetadata.BookMetadataBuilder()
                .isbn("978-80-8146-097-5c")
                .name("Hupsov šlabikár Lipka 2. časť")
                .build());
        bookMetadataMap.put("KOD_946", new BookMetadata.BookMetadataBuilder()
                .isbn("978-80-8091-359-5")
                .name("Administratíva a korešpondencia pre 2. ročník")
                .build());
        bookMetadataMap.put("KOD_947", new BookMetadata.BookMetadataBuilder()
                .isbn("978-80-8091-403-5")
                .name("")
                .build());
    }

    public String getIsbn(String ean) {
        BookMetadata bookMetadata = bookMetadataMap.get(ean);
        return bookMetadata == null ? null : bookMetadata.getIsbn();
    }

    public String getName(String ean) {
        BookMetadata bookMetadata = bookMetadataMap.get(ean);
        return bookMetadata == null ? null : bookMetadataMap.get(ean).getName();
    }

    public static class BookMetadata {

        private String isbn;

        private String name;

        public BookMetadata(BookMetadataBuilder builder) {
            this.isbn = builder.isbn;
            this.name = builder.name;
        }

        public String getIsbn() {
            return isbn;
        }

        public String getName() {
            return name;
        }

        public static class BookMetadataBuilder {

            private String isbn;

            private String name;

            public BookMetadataBuilder isbn(String isbn) {
                this.isbn = isbn;
                return this;
            }

            public BookMetadataBuilder name(String name) {
                this.name = name;
                return this;
            }

            public BookMetadata build() {
                return new BookMetadata(this);
            }
        }
    }
}
