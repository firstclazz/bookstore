package com.firstclazz.bookstore.repositories.entities;

import com.firstclazz.bookstore.repositories.BookRepository;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.UUID;

@Entity
@Table(name = "DELIVERY_ITEM")
public class DeliveryItem {

    @Id
    @Column(name = "ID", nullable = false)
    @Size(max = 36)
    private String id;

    @Column(name = "BOOK_ID")
    private String bookId;

    @Column(name = "BOOK_EAN")
    private String bookEan;

    @Column(name = "BOOK_ISBN")
    private String bookIsbn;

    @Column(name = "AMOUNT")
    private Integer amount;

    @Column(name = "ORDERED_AMOUNT")
    private Integer orderedAmount;

    public DeliveryItem() {
    }

    public DeliveryItem(DeliveryItem.DeliveryItemBuilder builder) {
        this.id = builder.id;
        this.bookId = builder.bookId;
        this.bookEan = builder.bookEan;
        this.bookIsbn = builder.bookIsbn;
        this.amount = builder.amount;
        this.orderedAmount = builder.orderedAmount;
    }

    public String getId() {
        return this.id;
    }

    public String getBookId() {
        return this.bookId;
    }

    public String getBookEan() {
        return this.bookEan;
    }

    public String getBookIsbn() {
        if (StringUtils.isBlank(bookIsbn)) {
            return BookRepository.getInstance().getIsbn(bookEan);
        }
        return bookIsbn;
    }

    public String getBookName() {
        return BookRepository.getInstance().getName(bookEan);
    }

    public Integer getAmount() {
        return this.amount;
    }

    public Integer getOrderedAmount() {
        return this.orderedAmount;
    }

    public static class DeliveryItemBuilder {
        private String id = UUID.randomUUID().toString();
        private String bookId;
        private String bookEan;
        private String bookIsbn;
        private Integer amount;
        private Integer orderedAmount;

        public DeliveryItemBuilder() {
        }

        public DeliveryItem.DeliveryItemBuilder id(String id) {
            this.id = id;
            return this;
        }

        public DeliveryItem.DeliveryItemBuilder bookId(String bookId) {
            this.bookId = bookId;
            return this;
        }

        public DeliveryItem.DeliveryItemBuilder bookEan(String bookEan) {
            this.bookEan = bookEan;
            return this;
        }

        public DeliveryItem.DeliveryItemBuilder bookIsbn(String bookIsbn) {
            this.bookIsbn = bookIsbn;
            return this;
        }

        public DeliveryItem.DeliveryItemBuilder amount(Integer amount) {
            this.amount = amount;
            return this;
        }

        public DeliveryItem.DeliveryItemBuilder orderedAmount(Integer orderedAmount) {
            this.orderedAmount = orderedAmount;
            return this;
        }

        public DeliveryItem build() {
            return new DeliveryItem(this);
        }
    }
}
