package com.firstclazz.bookstore.repositories.entities;

import com.firstclazz.bookstore.repositories.BookRepository;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "ORDER_ITEM")
public class OrderItem {

    @Id
    @Column(name = "ID", nullable = false)
    @Size(max = 36)
    private String id;

    @Column(name = "BOOK_ID")
    private String bookId;

    @Column(name = "BOOK_EAN")
    private String bookEan;

    @Column(name = "BOOK_ISBN")
    private String bookIsbn;

    @Column(name = "AMOUNT")
    private Integer amount;

    public OrderItem() {
    }

    public OrderItem(OrderItemBuilder builder) {
        this.id = builder.id;
        this.bookId = builder.bookId;
        this.bookEan = builder.bookEan;
        this.bookIsbn = builder.bookIsbn;
        this.amount = builder.amount;
    }

    public String getId() {
        return id;
    }

    public String getBookId() {
        return bookId;
    }

    public String getBookEan() {
        return bookEan;
    }

    public String getBookIsbn() {
        if (StringUtils.isBlank(bookIsbn)) {
            return BookRepository.getInstance().getIsbn(bookEan);
        }
        return bookIsbn;
    }

    public String getBookName() {
        return BookRepository.getInstance().getName(bookEan);
    }

    public Integer getAmount() {
        return amount;
    }

    public static class OrderItemBuilder {

        private String id;

        private String bookId;

        private String bookEan;

        private String bookIsbn;

        private Integer amount;

        public OrderItemBuilder id(String id) {
            this.id = id;
            return this;
        }

        public OrderItemBuilder bookId(String bookId) {
            this.bookId = bookId;
            return this;
        }

        public OrderItemBuilder bookEan(String bookEan) {
            this.bookEan = bookEan;
            return this;
        }

        public OrderItemBuilder bookIsbn(String bookIsbn) {
            this.bookIsbn = bookIsbn;
            return this;
        }

        public OrderItemBuilder amount (Integer amount) {
            this.amount = amount;
            return this;
        }

        public OrderItem build() {
            return new OrderItem(this);
        }

    }

}
