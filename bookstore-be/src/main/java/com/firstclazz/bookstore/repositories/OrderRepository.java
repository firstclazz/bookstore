package com.firstclazz.bookstore.repositories;

import com.firstclazz.bookstore.repositories.entities.EGovOrder;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<EGovOrder, String>, OrderRepositoryCustom {
}
