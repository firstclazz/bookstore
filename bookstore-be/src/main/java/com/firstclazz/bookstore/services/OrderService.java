package com.firstclazz.bookstore.services;

import com.firstclazz.bookstore.repositories.OrderRepository;
import com.firstclazz.bookstore.repositories.entities.EGovOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private PdfGeneratorService pdfGeneratorService;

    @Autowired
    private XlsGeneratorService xlsGeneratorService;

    public List<EGovOrder> getOrders(Integer page, Integer pageSize, String sortColumn,
                                     String sortDir, String status, String code,
                                     String address, String street, String municipality) {
        return this.orderRepository.findOrders(page, pageSize, sortColumn, sortDir,
                status, code, address, street, municipality);
    }

    public Long getOrdersCount(String status, String code, String address, String street, String municipality) {
        return this.orderRepository.getOrdersCount(status, code, address, street, municipality);
    }

    public EGovOrder getOrderDetail(String id) {
        return (EGovOrder)this.orderRepository.findOne(id);
    }

    public String getOrdersPdf(String sortColumn, String sortDir, String status, String code,
                               String address, String street, String municipality) {
        List<EGovOrder> orders = getOrders(null, null, sortColumn, sortDir,
                status, code, address, street, municipality);
        return pdfGeneratorService.generateOrders(orders);
    }

    public String getOrdersXls(String sortColumn, String sortDir, String status, String code,
                               String address, String street, String municipality) {
        List<EGovOrder> orders = getOrders(null, null, sortColumn, sortDir,
                status, code, address, street, municipality);
        return xlsGeneratorService.generateOrders(orders);
    }

}
