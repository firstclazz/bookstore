package com.firstclazz.bookstore.services;

import com.firstclazz.bookstore.repositories.entities.Delivery;
import com.firstclazz.bookstore.repositories.entities.EGovOrder;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;

@Service
public class XlsGeneratorService {

    public String generateOrders(List<EGovOrder> orders) {

        try {

            // Initialize the file name
            String filePath = "/tmp/" + UUID.randomUUID().toString() + ".xlsx";

            // Initialize XLS file
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet("Zoznam objednávok");

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

            // Print header
            CellStyle headerStyle = workbook.createCellStyle();
            headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            int rowNum = 0;
            Row row = sheet.createRow(rowNum++);
            createCell(row, 0, headerStyle).setCellValue("Kód objednávky");
            createCell(row, 1, headerStyle).setCellValue("Adresa");
            createCell(row, 2, headerStyle).setCellValue("Ulica");
            createCell(row, 3, headerStyle).setCellValue("Mesto");
            createCell(row, 4, headerStyle).setCellValue("Dátum vytvorenia");
            createCell(row, 5, headerStyle).setCellValue("Stav");
            createCell(row, 6, headerStyle).setCellValue("Položiek");

            // Print data
            if (orders != null) {
                for (EGovOrder order : orders) {
                    row = sheet.createRow(rowNum++);
                    row.createCell(0).setCellValue(order.getCode());
                    row.createCell(1).setCellValue(order.getAddress());
                    row.createCell(2).setCellValue(order.getStreet());
                    row.createCell(3).setCellValue(order.getMunicipality());
                    row.createCell(4).setCellValue(order.getCreated() == null ? "" : dateFormat.format(order.getCreated()).toString());
                    row.createCell(5).setCellValue(order.getStatusName());
                    row.createCell(6).setCellValue(order.getOrderItemsCount());
                }
            }

            FileOutputStream outputStream = new FileOutputStream(filePath);
            workbook.write(outputStream);
            workbook.close();

            return filePath;

        } catch (Exception e) {
            // TODO exception
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    public String generateDeliveryNotes(List<Delivery> deliveries) {

        try {

            // Initialize the file name
            String filePath = "/tmp/" + UUID.randomUUID().toString() + ".xlsx";

            // Initialize XLS file
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet("Zoznam dodacích listov");

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

            // Print header
            CellStyle headerStyle = workbook.createCellStyle();
            headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            int rowNum = 0;
            Row row = sheet.createRow(rowNum++);

            createCell(row, 0, headerStyle).setCellValue("Číslo dodacieho listu");
            createCell(row, 1, headerStyle).setCellValue("Kód objednávky");
            createCell(row, 2, headerStyle).setCellValue("Adresa");
            createCell(row, 3, headerStyle).setCellValue("Ulica");
            createCell(row, 4, headerStyle).setCellValue("Mesto");
            createCell(row, 5, headerStyle).setCellValue("Položiek");

            // Print data
            if (deliveries != null) {
                for (Delivery delivery : deliveries) {
                    row = sheet.createRow(rowNum++);
                    row.createCell(0).setCellValue(delivery.getDeliveryNumber());
                    row.createCell(1).setCellValue(delivery.getOrder().getStatusName());
                    row.createCell(2).setCellValue(delivery.getOrder().getAddress());
                    row.createCell(3).setCellValue(delivery.getOrder().getStreet());
                    row.createCell(4).setCellValue(delivery.getOrder().getMunicipality());
                    row.createCell(5).setCellValue(delivery.getOrder().getOrderItemsCount());
                }
            }

            FileOutputStream outputStream = new FileOutputStream(filePath);
            workbook.write(outputStream);
            workbook.close();

            return filePath;

        } catch (Exception e) {
            // TODO exception
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    private Cell createCell(Row row, int index, CellStyle style) {
        Cell cell  = row.createCell(index);
        cell.setCellStyle(style);
        return cell;
    }

}
