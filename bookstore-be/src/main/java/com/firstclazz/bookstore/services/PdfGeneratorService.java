package com.firstclazz.bookstore.services;

import com.firstclazz.bookstore.repositories.entities.Delivery;
import com.firstclazz.bookstore.repositories.entities.DeliveryItem;
import com.firstclazz.bookstore.repositories.entities.EGovOrder;
import com.firstclazz.bookstore.repositories.entities.OrderItem;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.UnitValue;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;


@Service
public class PdfGeneratorService {

    public String generateDeliveryNotice(Delivery delivery) {

        try {

            //Initialize PDF writer
            String filePath = "/tmp/" + delivery.getId()+ ".pdf";
            PdfWriter writer = new PdfWriter(filePath);

            //Initialize PDF document
            PdfDocument pdf = new PdfDocument(writer);

            // Font
            PdfFont font = PdfFontFactory.createFont(FontConstants.HELVETICA, "Cp1250");

            // Initialize document
            Document document = new Document(pdf, PageSize.A4)
                    .setFont(font)
                    .setFontSize(10);

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

            // Header
            document.add(new Paragraph("Dodací list číslo: " + delivery.getDeliveryNumber()).setBold().setFontSize(14));
            document.add(new Paragraph("Kód objednávky: " + delivery.getOrder().getCode()));
            document.add(new Paragraph("Dátum vystavenia objednávky: "
                    + (delivery.getOrder().getCreated() == null ? "" : dateFormat.format(delivery.getOrder().getCreated())))
                    .setFixedLeading(5));
            document.add(new Paragraph("Dátum schválenia objednávky: "
                    + (delivery.getOrder().getConfirmed() == null ? "" : dateFormat.format(delivery.getOrder().getConfirmed())))
                    .setFixedLeading(5));

            Table table = new Table(new UnitValue[] {
                    new UnitValue(50, UnitValue.PERCENT),
                    new UnitValue(50, UnitValue.PERCENT)
            });
            table.setWidthPercent(100);
            table.setBorder(Border.NO_BORDER);
            table.setMarginTop(15);

            table.addCell(new Cell().add("Objednávateľ").setBold().setBorder(Border.NO_BORDER));
            table.addCell(new Cell().add("Dodávateľ").setBold().setBorder(Border.NO_BORDER));

            table.addCell(new Cell().add(delivery.getOrder().getAddress()).setBorder(Border.NO_BORDER));
            table.addCell(new Cell().add("GGT a.s.").setBorder(Border.NO_BORDER));

            table.addCell(new Cell().add(delivery.getOrder().getStreet()).setBorder(Border.NO_BORDER));
            table.addCell(new Cell().add("Stará Vajnorská 9").setBorder(Border.NO_BORDER));

            table.addCell(new Cell().add(delivery.getOrder().getPostalCode() + " " + delivery.getOrder().getMunicipality()).setBorder(Border.NO_BORDER));
            table.addCell(new Cell().add("831 04 Bratislava").setBorder(Border.NO_BORDER));

            table.addCell(new Cell().add(delivery.getOrder().getDistrict()).setBorder(Border.NO_BORDER));
            table.addCell(new Cell().add("Bratislava").setBorder(Border.NO_BORDER));

            table.addCell(new Cell().add("IČO: " + delivery.getOrder().getSchoolCode()).setBorder(Border.NO_BORDER));
            table.addCell(new Cell().add("IČO: 35 791 829" + delivery.getOrder().getSchoolCode()).setBorder(Border.NO_BORDER));

            document.add(table);

            Table itemsTable = new Table(new UnitValue[] {
                    new UnitValue(20, UnitValue.PERCENT),
                    new UnitValue(40, UnitValue.PERCENT),
                    new UnitValue(20, UnitValue.PERCENT),
                    new UnitValue(20, UnitValue.PERCENT),
            });
            itemsTable.setWidthPercent(100);
            itemsTable.setMarginTop(20);
            itemsTable.addHeaderCell(new Cell().add("EAN Kód").setBold().setBackgroundColor(Color.LIGHT_GRAY));
            itemsTable.addHeaderCell(new Cell().add("Názov").setBold().setBackgroundColor(Color.LIGHT_GRAY));
            itemsTable.addHeaderCell(new Cell().add("ISBN Kód").setBold().setBackgroundColor(Color.LIGHT_GRAY));
            itemsTable.addHeaderCell(new Cell().add("Množstvo").setBold().setBackgroundColor(Color.LIGHT_GRAY));

            if (delivery.getDeliveryItems() != null) {
                for (DeliveryItem item : delivery.getDeliveryItems()) {
                    itemsTable.addCell(item.getBookEan() == null ? "" : item.getBookEan());
                    itemsTable.addCell(item.getBookName() == null ? "" : item.getBookName());
                    itemsTable.addCell(item.getBookIsbn() == null ? "" : item.getBookIsbn());
                    itemsTable.addCell(item.getAmount() == null ? "" : item.getAmount().toString());
                }
            }

            document.add(itemsTable);

            // Close document
            document.close();

            return filePath;

        } catch (Exception e) {
            // TODO exception
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public String generateOrders(List<EGovOrder> orders) {
        try {

            //Initialize PDF writer
            String filePath = "/tmp/" + UUID.randomUUID().toString() + ".pdf";
            PdfWriter writer = new PdfWriter(filePath);

            //Initialize PDF document
            PdfDocument pdf = new PdfDocument(writer);

            // Font
            PdfFont font = PdfFontFactory.createFont(FontConstants.HELVETICA, "Cp1250");

            // Initialize document
            Document document = new Document(pdf, PageSize.A4)
                    .setFont(font)
                    .setFontSize(10);

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

            // Header
            document.add(new Paragraph("Zoznam objednávok").setBold().setFontSize(14));
            Table itemsTable = new Table(new UnitValue[] {
                    new UnitValue(13, UnitValue.PERCENT),
                    new UnitValue(23, UnitValue.PERCENT),
                    new UnitValue(13, UnitValue.PERCENT),
                    new UnitValue(13, UnitValue.PERCENT),
                    new UnitValue(13, UnitValue.PERCENT),
                    new UnitValue(13, UnitValue.PERCENT),
                    new UnitValue(12, UnitValue.PERCENT),
            });
            itemsTable.setWidthPercent(100);
            itemsTable.setMarginTop(20);
            itemsTable.addHeaderCell(new Cell().add("Kód objednávky").setBold().setBackgroundColor(Color.LIGHT_GRAY));
            itemsTable.addHeaderCell(new Cell().add("Adresa").setBold().setBackgroundColor(Color.LIGHT_GRAY));
            itemsTable.addHeaderCell(new Cell().add("Ulica").setBold().setBackgroundColor(Color.LIGHT_GRAY));
            itemsTable.addHeaderCell(new Cell().add("Mesto").setBold().setBackgroundColor(Color.LIGHT_GRAY));
            itemsTable.addHeaderCell(new Cell().add("Dátum vytvorenia").setBold().setBackgroundColor(Color.LIGHT_GRAY));
            itemsTable.addHeaderCell(new Cell().add("Stav").setBold().setBackgroundColor(Color.LIGHT_GRAY));
            itemsTable.addHeaderCell(new Cell().add("Položiek").setBold().setBackgroundColor(Color.LIGHT_GRAY));

            if (orders != null) {
                for (EGovOrder order : orders) {
                    itemsTable.addCell(order.getCode());
                    itemsTable.addCell(order.getAddress());
                    itemsTable.addCell(order.getStreet());
                    itemsTable.addCell(order.getMunicipality());
                    itemsTable.addCell(order.getCreated() == null ? "" : dateFormat.format(order.getCreated()).toString());
                    itemsTable.addCell(order.getStatusName());
                    itemsTable.addCell(order.getOrderItemsCount().toString());
                }
            }

            document.add(itemsTable);

            // Close document
            document.close();

            return filePath;

        } catch (Exception e) {
            // TODO exception
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public String generateDeliveryNotes(List<Delivery> deliveries) {
        try {

            //Initialize PDF writer
            String filePath = "/tmp/" + UUID.randomUUID().toString() + ".pdf";
            PdfWriter writer = new PdfWriter(filePath);

            //Initialize PDF document
            PdfDocument pdf = new PdfDocument(writer);

            // Font
            PdfFont font = PdfFontFactory.createFont(FontConstants.HELVETICA, "Cp1250");

            // Initialize document
            Document document = new Document(pdf, PageSize.A4)
                    .setFont(font)
                    .setFontSize(10);

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

            // Header
            document.add(new Paragraph("Zoznam dodacích listov").setBold().setFontSize(14));
            Table itemsTable = new Table(new UnitValue[] {
                    new UnitValue(15, UnitValue.PERCENT),
                    new UnitValue(25, UnitValue.PERCENT),
                    new UnitValue(15, UnitValue.PERCENT),
                    new UnitValue(15, UnitValue.PERCENT),
                    new UnitValue(15, UnitValue.PERCENT),
                    new UnitValue(15, UnitValue.PERCENT),
            });
            itemsTable.setWidthPercent(100);
            itemsTable.setMarginTop(20);
            itemsTable.addHeaderCell(new Cell().add("Číslo dodacieho listu").setBold().setBackgroundColor(Color.LIGHT_GRAY));
            itemsTable.addHeaderCell(new Cell().add("Kód objednávky").setBold().setBackgroundColor(Color.LIGHT_GRAY));
            itemsTable.addHeaderCell(new Cell().add("Adresa").setBold().setBackgroundColor(Color.LIGHT_GRAY));
            itemsTable.addHeaderCell(new Cell().add("Ulica").setBold().setBackgroundColor(Color.LIGHT_GRAY));
            itemsTable.addHeaderCell(new Cell().add("Mesto").setBold().setBackgroundColor(Color.LIGHT_GRAY));
            itemsTable.addHeaderCell(new Cell().add("Položiek").setBold().setBackgroundColor(Color.LIGHT_GRAY));

            if (deliveries != null) {
                for (Delivery delivery : deliveries) {
                    itemsTable.addCell(delivery.getDeliveryNumber());
                    itemsTable.addCell(delivery.getOrder().getStatusName());
                    itemsTable.addCell(delivery.getOrder().getAddress());
                    itemsTable.addCell(delivery.getOrder().getStreet());
                    itemsTable.addCell(delivery.getOrder().getMunicipality());
                    itemsTable.addCell(delivery.getOrder().getOrderItemsCount().toString());
                }
            }

            document.add(itemsTable);

            // Close document
            document.close();

            return filePath;

        } catch (Exception e) {
            // TODO exception
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

}
