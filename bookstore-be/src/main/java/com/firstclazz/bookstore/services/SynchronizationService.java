package com.firstclazz.bookstore.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.firstclazz.bookstore.infrastructure.factories.GovMessageFactory;
import com.firstclazz.bookstore.infrastructure.models.GovArrayOfOrder;
import com.firstclazz.bookstore.infrastructure.models.GovMessage;
import com.firstclazz.bookstore.repositories.ConfigRepository;
import com.firstclazz.bookstore.repositories.DeliveryRepository;
import com.firstclazz.bookstore.repositories.OrderRepository;
import com.firstclazz.bookstore.repositories.entities.Config;
import com.firstclazz.bookstore.repositories.entities.ConfigEnum;
import com.firstclazz.bookstore.repositories.entities.Delivery;
import com.firstclazz.bookstore.repositories.entities.EGovOrder;
import com.firstclazz.bookstore.repositories.factories.OrderFactory;
import com.sun.org.apache.xerces.internal.dom.ElementNSImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import sk.gov.edu.dataservices.DataService;
import sk.gov.edu.egovmessage.EGovMessage;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@Service
public class SynchronizationService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private DeliveryRepository deliveryRepository;

    @Autowired
    private ConfigRepository configRepository;

    public static Logger LOG = LoggerFactory.getLogger(SynchronizationService.class);

    @Scheduled(cron = "${scheduler.order.cron}")
    public void syncOrdersScheduler() {
        LOG.info("Starting deliveries synchronization process");
        syncDeliveries();
        LOG.info("Synchronization process of deliveries has been done");

        LOG.info("Starting order synchronization process");
        syncOrders();
        LOG.info("Synchronization process of orders has been done");
    }

    private void syncDeliveries() {
        List<Delivery> deliveries = deliveryRepository.findBySyncedIsFalseAndConfirmedIsNotNull();
        if (deliveries != null && deliveries.size() >0) {
            DataService dataService = new DataService();
            GovMessageFactory factory = new GovMessageFactory();
            EGovMessage request = factory.setDeliveryNoticeRequest(deliveries);
            EGovMessage result = dataService.getBasicHttpBindingIEGovDataService().getData(request);
            if ("OK".equalsIgnoreCase(result.getBody().getResult().getCode())) {
                for (Delivery delivery : deliveries) {
                    delivery.setSynced(true);
                    deliveryRepository.save(delivery);
                }
            } else {
                LOG.error("Synchronization of the deliveries failed with error: "
                        + result.getBody().getResult().getMessage());
            }
        }
    }

    private static String jaxbObjectToXML(EGovMessage message) {
        String xmlString = "";
        try {
            JAXBContext context = JAXBContext.newInstance(EGovMessage.class, GovMessage.class);
            Marshaller m = context.createMarshaller();

            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); // To format XML

            StringWriter sw = new StringWriter();
            m.marshal(message, sw);
            xmlString = sw.toString();

        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return xmlString;
    }

    public void syncOrders() {

        int page = 1;
        int pageSize = 100;
        boolean eof = false;

        // Get date of the last synchronization
        Date lastSyncDate = getLastSync();

        // Decrease lastSyncDate about 2 hours
        if (lastSyncDate != null) {
            Calendar lastSyncCalendar = new GregorianCalendar();
            lastSyncCalendar.setTime(lastSyncDate);
            lastSyncCalendar.add(Calendar.HOUR, -2);
            lastSyncDate = lastSyncCalendar.getTime();
            LOG.info("Starting incremental synchronization process from: " + lastSyncDate);
        } else {
            LOG.info("Starting initial synchronization process");
        }

        DataService dataService = new DataService();
        while (!eof) {
            GovMessageFactory factory = new GovMessageFactory();
            EGovMessage request = factory.createOrdersRequest(page, pageSize, lastSyncDate);
            EGovMessage result = dataService.getBasicHttpBindingIEGovDataService().getData(request);
            String json = extractJsonAsString(result);
            List<GovArrayOfOrder.Order> govOrders = mapOrdersJsonToObjects(json);
            // NULL or EMPTY results
            if (govOrders == null || govOrders.size() == 0) {
                return;
            }
            // Store orders into database
            for (GovArrayOfOrder.Order govOrder : govOrders) {
                OrderFactory orderFactory = new OrderFactory();
                EGovOrder order = orderFactory.createOrderFromGovOrder(govOrder);
                orderRepository.merge(order);
            }
            if (govOrders.size() < pageSize) {
                eof = true;
            }
            page++;
        }

        setLastSync();

    }

    private Date getLastSync() {
        Config lastSync = configRepository.findOne(ConfigEnum.LAST_SYNC.name());
        return lastSync == null ? null : lastSync.getDateValue();
    }

    private void setLastSync() {
        Config lastSync = new Config.ConfigBuilder(ConfigEnum.LAST_SYNC)
                .dateValue(new Date())
                .build();
        configRepository.save(lastSync);
    }

    private String extractJsonAsString(EGovMessage message) {
        String json = "";
        if (message.getBody().getData().getAny() != null
                && message.getBody().getData().getAny() instanceof ElementNSImpl) {
            json = ((ElementNSImpl) message.getBody().getData().getAny()).getFirstChild().getNodeValue();
        }
        return json;
    }

    private List<GovArrayOfOrder.Order> mapOrdersJsonToObjects(String json) {
        try {
            ObjectMapper mapper = new ObjectMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            List<GovArrayOfOrder.Order> orders = mapper.readValue(json,
                    new TypeReference<List<GovArrayOfOrder.Order>>() {});
            return orders;
        } catch (IOException e) {
            // TODO excepiton handling
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

}
