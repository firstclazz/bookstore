package com.firstclazz.bookstore.services;

import com.firstclazz.bookstore.facade.models.UserModel;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    public UserModel getLoggedUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String login = null;
        if (principal instanceof User) {
            login = ((User) principal).getUsername();
        }

        return new UserModel.UserModelBuilder()
                .login(login)
                .build();
    }

    public String getLoggedUserName() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String login = null;
        if (principal instanceof User) {
            login = ((User) principal).getUsername();
        }
        return login;
    }
}
