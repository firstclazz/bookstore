package com.firstclazz.bookstore.services;

import com.firstclazz.bookstore.repositories.DeliveryRepository;
import com.firstclazz.bookstore.repositories.OrderRepository;
import com.firstclazz.bookstore.repositories.entities.Delivery;
import com.firstclazz.bookstore.repositories.entities.DeliveryItem;
import com.firstclazz.bookstore.repositories.entities.EGovOrder;
import com.firstclazz.bookstore.repositories.entities.OrderItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Service
public class DeliveryService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private DeliveryRepository deliveryRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private PdfGeneratorService pdfGeneratorService;

    @Autowired
    private XlsGeneratorService xlsGeneratorService;

    public static final Logger LOG = LoggerFactory.getLogger(DeliveryService.class);

    public DeliveryService() {
    }

    public String createDelivery(String orderId) {

        Delivery oldDelivery = deliveryRepository.findOne(orderId);
        if (oldDelivery != null) {
            // TODO
            LOG.warn("Delivery note for order + " + orderId + " already exist. Skipping creation process.");
            return oldDelivery.getId();
        }

        EGovOrder order = orderRepository.findOne(orderId);

        Delivery delivery = new Delivery.DeliveryBuilder()
                .id(order.getId())
                .deliveryNumber("DL_" + order.getCode())
                .createdBy(userService.getLoggedUserName())
                .created(new Date())
                .order(order)
                .synced(false)
                .build();

        List<DeliveryItem> deliveryItems = new ArrayList();
        if (order.getOrderItems() != null) {
            for (OrderItem orderItem : order.getOrderItems()) {
                DeliveryItem deliveryItem = new DeliveryItem.DeliveryItemBuilder()
                        .id(orderItem.getId())
                        .bookId(orderItem.getBookId())
                        .bookEan(orderItem.getBookEan())
                        .amount(orderItem.getAmount())
                        .orderedAmount(orderItem.getAmount())
                        .build();
                deliveryItems.add(deliveryItem);
            }
        }

        delivery.setDeliveryItems(deliveryItems);
        deliveryRepository.save(delivery);
        order.setDeliveryCreated(true);
        orderRepository.save(order);

        return delivery.getId();

    }

    public void updateDelivery(String deliveryId, Delivery delivery) {
        // TODO validate inputs
        deliveryRepository.save(delivery);
    }

    public void confirmDelivery(String deliveryId, Delivery delivery) {
        // TODO validate inputs
        delivery.setConfirmed(new Date());
        delivery.setConfirmedBy(userService.getLoggedUserName());
        deliveryRepository.save(delivery);
    }

    public List<Delivery> getDeliveries(Integer page, Integer size) {
        return deliveryRepository.findDeliveries(page, size);
    }

    public Long getDeliveryCount() {
        return deliveryRepository.getDeliveryCount();
    }

    public Delivery getDeliveryDetail(String id) {
        return (Delivery) deliveryRepository.findOne(id);
    }

    public String getDeliveryNotePdf(String id) {
        Delivery delivery = getDeliveryDetail(id);
        return pdfGeneratorService.generateDeliveryNotice(delivery);
    }

    public String getDeliveryNotesPdf() {
        List<Delivery> deliveries = getDeliveries(null, null);
        return pdfGeneratorService.generateDeliveryNotes(deliveries);
    }

    public String getDeliveryNotesXls() {
        List<Delivery> deliveries = getDeliveries(null, null);
        return xlsGeneratorService.generateDeliveryNotes(deliveries);
    }

}
